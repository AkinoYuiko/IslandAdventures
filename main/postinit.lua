--Update this list when adding files
local components_post = {
    "actionqueuer",
    "ambientsound",
    "amphibiouscreature",
    "areaaware",
    "birdspawner",
    "builder",
    "burnable",
    "childspawner",
    "colourcube",
    "combat",
    "cookable",
    "crop",
    "deployable",
    "drownable",
    "dryer",
    "dynamicmusic",
    "eater",
    "edible",
    "equippable",
    "explosive",
    "fertilizer",
    "firedetector",
    "fishable",
    "fishingrod",
    "floater",
    "foodmemory",
    "frograin",
    "frostybreather",
    "fuel",
    "fueled",
    "follower",
    "growable",
    "health",
    "herdmember",
    "hounded",
    --"hunter",
    "inspectable",
    "inventory",
    "inventoryitem",
    "inventoryitemmoisture",
    "leader",
    "lighter",
    "locomotor",
    "lootdropper",
    "lureplantspawner",
    "minionspawner",
    "moisture",
    "oceancolor",
    "oldager",
	"penguinspawner",
    "perishable",
	"pickable",
    "placer",
    "playeractionpicker",
    "playercontroller",
    "playerspawner",
    "recallmark",
    "regrowthmanager",
    "repairable",
    "shadowcreaturespawner",
    "sheltered",
	"spell",
    "stackable",
    "stewer",
	"tackler",
    "teamleader",
    "temperature",
    "thief",
    "trap",
    "uianim",
    "weather",
    "weapon",
    "wildfires",
    "wisecracker",
    "witherable",
    "worldstate",
    "worldwind",
}

local prefabs_post = {
    "appeasement_item",
    "ash",
    "balloonvest",
    --"bernie",
    "birdcage",
    "book_birds",
    "book_gardening",
    "buff_workeffectiveness",
    "cactus",
    "campfire",
    "cave_entrance",
    "chester",
    "chester_eyebone",
    "cookpot",
    "dirtpile",
    "eel",
    "fireflies",
    "firesuppressor",
    "fish",
    "float_fx",
    "gears",
    "gestalt",
    "glasscutter",
	"grass",
    "healthregenbuff",
    "heatrock",
    "houndwarning",
    "icebox",
    "inventoryitem_classified",
    "lantern",
    "lighter",
    "lightning",
	"lureplant",
    "oceanfish",
    "mandrake",
    "marsh_bush",
    "meatrack",
    "meats",
    "merm",
    "mermhouse",
    "mermhouse_crafted",
    --"minisign",
    "player_classified",
    "pocketwatch",
    "poison_immune",
    "portablecookpot",
    "prototyper",
    "rainometer",
    "resurrectionstone",
    "sapling",
    "sewing_tape",
    "shadowmeteor",
    "shadowwaxwell",
    "tentacle",
    "thunder_close",
    "thunder_far",
    "torch",
    "trees",
    "trinkets",
	"trophyscale_fish",
	"variants_ia",
    "volcanolevel",
    "warly",
    "warningshadow",
    "walls",
    "winterometer",
    "wobster",
    --"woby",
    "wolfgang",
    "woodie",
    "world",
    "wormwood_plant_fx",
    "wurt",
	"wx78_scanner",
	"wx78_scanner_item",
    "wx78",
}

local scenarios_post = {
    "chestfunctions",
}

local stategraphs_post = {
    "commonstates",
    "shadowwaxwell",
    "bird",
    "frog",
    "merm",
    "shadowcreature",
    "shadowwaxwell",
    "wilson",
    "wilson_client",
}

local brains_post = {
    "crabkingclawbrain",
    "shadowwaxwellbrain",
}

local class_post = {
    "components/builder_replica",
    "components/combat_replica",
    "components/equippable_replica",
    "components/inventoryitem_replica",
    "screens/playerhud",
    "widgets/redux/craftingmenu_widget",
    "widgets/containerwidget",
    "widgets/healthbadge",
    "widgets/inventorybar",
    "widgets/itemtile",
    "widgets/mapwidget",
    "widgets/seasonclock",
    "widgets/widget",
}

modimport("postinit/sim")
modimport("postinit/any")
modimport("postinit/player")
modimport("postinit/writeables")

for _,v in pairs(components_post) do
    modimport("postinit/components/"..v)
end

for _,v in pairs(prefabs_post) do
    modimport("postinit/prefabs/"..v)
end

for _,v in pairs(scenarios_post) do
    modimport("postinit/scenarios/"..v)
end

for _,v in pairs(stategraphs_post) do
    modimport("postinit/stategraphs/SG"..v)
end

for _,v in pairs(brains_post) do
    modimport("postinit/brains/"..v)
end

for _,v in pairs(class_post) do
    --These contain a path already, e.g. v= "widgets/inventorybar"
    modimport("postinit/".. v)
end
