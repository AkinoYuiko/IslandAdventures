local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local cooking = require("cooking")
require("util")

local COMMON = 3
local UNCOMMON = 1
local RARE = .5

-- IA_VEGGIES = {
--     sweet_potato = {
--         seed_weight = COMMON,
--         health = TUNING.HEALING_TINY,
--         hunger = TUNING.CALORIES_SMALL,
--         sanity = 0,
--         perishtime = TUNING.PERISH_MED,
--         cooked_health = TUNING.HEALING_SMALL,
--         cooked_hunger = TUNING.CALORIES_SMALL,
--         cooked_perishtime = TUNING.PERISH_FAST,
--         cooked_sanity = 0,
--     }
-- }
local function MakeGrowTimes(germination_min, germination_max, full_grow_min, full_grow_max)
	local grow_time = {}

	-- germination time
	grow_time.seed		= {germination_min, germination_max}

	-- grow time
	grow_time.sprout	= {full_grow_min * 0.5, full_grow_max * 0.5}
	grow_time.small		= {full_grow_min * 0.3, full_grow_max * 0.3}
	grow_time.med		= {full_grow_min * 0.2, full_grow_max * 0.2}

	-- harvestable perish time
	grow_time.full		= 4 * TUNING.TOTAL_DAY_TIME
	grow_time.oversized	= 6 * TUNING.TOTAL_DAY_TIME
	grow_time.regrow	= {4 * TUNING.TOTAL_DAY_TIME, 5 * TUNING.TOTAL_DAY_TIME} -- min, max

	return grow_time
end

local drink_low = TUNING.FARM_PLANT_DRINK_LOW
local drink_med = TUNING.FARM_PLANT_DRINK_MED
local drink_high = TUNING.FARM_PLANT_DRINK_HIGH
-- Nutrients
local S = TUNING.FARM_PLANT_CONSUME_NUTRIENT_LOW
local M = TUNING.FARM_PLANT_CONSUME_NUTRIENT_MED
local L = TUNING.FARM_PLANT_CONSUME_NUTRIENT_HIGH
local PLANT_DEFS = require("prefabs/farm_plant_defs").PLANT_DEFS
PLANT_DEFS.sweet_potato = {
	build = "farm_plant_sweet_potato", 
	bank = "farm_plant_sweet_potato",
	
	grow_time = MakeGrowTimes(12 * TUNING.SEG_TIME, 16 * TUNING.SEG_TIME,		4 * TUNING.TOTAL_DAY_TIME, 7 * TUNING.TOTAL_DAY_TIME),

	moisture = {drink_rate = drink_low,	min_percent = TUNING.FARM_PLANT_DROUGHT_TOLERANCE},

	good_seasons = {autumn = true,	winter = true,	spring = true},

	nutrient_consumption = {M, 0, 0},

	nutrient_restoration = {true,true,false},

	max_killjoys_tolerance	= TUNING.FARM_PLANT_KILLJOY_TOLERANCE,

	weight_data	= { 365.56,     987.65,     0.28 },

	prefab = "farm_plant_sweet_potato",

	product = "sweet_potato",

	product_oversized = "sweet_potato_oversized",

	seed = "sweet_potato_seeds",

	plant_type_tag = "farm_plant_sweet_potato",

	loot_oversized_rot = {"spoiled_food", "spoiled_food", "spoiled_food", "sweet_potato_seeds", "fruitfly", "fruitfly"},

	family_min_count = TUNING.FARM_PLANT_SAME_FAMILY_MIN,

	family_check_dist = TUNING.FARM_PLANT_SAME_FAMILY_RADIUS,

	stage_netvar = net_tinybyte,

	sounds = PLANT_DEFS.pumpkin.sounds,
	
	plantregistryinfo = {
		{
			text = "seed",
			anim = "crop_seed",
			grow_anim = "grow_seed",
			learnseed = true,
			growing = true,
		},
		{
			text = "sprout",
			anim = "crop_sprout",
			grow_anim = "grow_sprout",
			growing = true,
		},
		{
			text = "small",
			anim = "crop_small",
			grow_anim = "grow_small",
			growing = true,
		},
		{
			text = "medium",
			anim = "crop_med",
			grow_anim = "grow_med",
			growing = true,
		},
		{
			text = "grown",
			anim = "crop_full",
			grow_anim = "grow_full",
			revealplantname = true,
			fullgrown = true,
		},
		{
			text = "oversized",
			anim = "crop_oversized",
			grow_anim = "grow_oversized",
			revealplantname = true,
			fullgrown = true,
		},
		{
			text = "rotting",
			anim = "crop_rot",
			grow_anim = "grow_rot",
			stagepriority = -100,
			is_rotten = true,
			hidden = true,
		},
		{
			text = "oversized_rotting",
			anim = "crop_rot_oversized",
			grow_anim = "grow_rot_oversized",
			stagepriority = -100,
			is_rotten = true,
			hidden = true,
		},
	},
	plantregistrywidget = "widgets/redux/farmplantpage",
	plantregistrysummarywidget = "widgets/redux/farmplantsummarywidget",
	pictureframeanim = {anim = "emoteXL_happycheer", time = 0.5} ,
}

IAENV.AddIngredientValues({"seaweed"}, {veggie=1}, true, true)
IAENV.AddIngredientValues({"sweet_potato"}, {veggie=1}, true)
IAENV.AddIngredientValues({"coffeebeans"}, {fruit=.5})
IAENV.AddIngredientValues({"coffeebeans_cooked"}, {fruit=1})
IAENV.AddIngredientValues({"coconut_cooked", "coconut_halved"}, {fruit=1,fat=1})
IAENV.AddIngredientValues({"doydoyegg"}, {egg=1}, true)
IAENV.AddIngredientValues({"dorsalfin"}, {inedible=1})
IAENV.AddIngredientValues({"shark_fin", "fish_tropical", --[["pondfish_tropical",--]] "solofish_dead", "swordfish_dead"}, {meat=0.5,fish=1})
IAENV.AddIngredientValues({--[["fish_med",--]] "roe", "purple_grouper", "pondpurple_grouper", "pierrot_fish", "pondpierrot_fish", "neon_quattro", "pondneon_quattro"}, {meat=0.5,fish=1}, true)
--IAENV.AddIngredientValues({"fish_small", "fish_small"}, {fish=0.5}, true)
IAENV.AddIngredientValues({"jellyfish", "jellyfish_dead", "jellyfish_cooked", "jellyjerky"}, {fish=1,jellyfish=1,monster=1})
IAENV.AddIngredientValues({"limpets", "mussel"}, {fish=.5}, true)
IAENV.AddIngredientValues({"lobster"}, {fish=2}, true)
IAENV.AddIngredientValues({"crab"}, {fish=.5})
IAENV.AddIngredientValues({"pondfish_tropical"}, cooking.ingredients["pondfish"].tags)

cooking.GetRecipe("cookpot", "batnosehat").test = function(cooker, names, tags) return names.batnose and (names.kelp or names.seaweed) and (tags.dairy and tags.dairy >= 1) end
cooking.GetRecipe("cookpot", "californiaroll").test = function(cooker, names, tags) return (names.seaweed and names.seaweed == 2 or names.kelp and names.kelp == 2 or names.kelp and names.seaweed) and (tags.fish and tags.fish >= 1) end
cooking.GetRecipe("cookpot", "barnaclesushi").test = function(cooker, names, tags) return (names.barnacle or names.barnacle_cooked) and (names.kelp or names.kelp_cooked or names.seaweed or names.seaweed_cooked) and tags.egg and tags.egg >= 1 end
cooking.GetRecipe("cookpot", "barnaclestuffedfishhead").test = function(cooker, names, tags) return (names.barnacle or names.barnacle_cooked or names.mussel or names.mussel_cooked) and tags.fish and tags.fish >= 1.25 end
local foods = {

--[[
	californiaroll =
	{
		test = function(cooker, names, tags) return (names.seaweed and names.seaweed == 2 or names.kelp and names.kelp == 2 or names.kelp and names.seaweed) and (tags.fish and tags.fish >= 1) end,
		priority = 20,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_MED,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_SMALL,
		cooktime = .5,
	},

	seafoodgumbo =
	{
		test = function(cooker, names, tags) return tags.fish and tags.fish > 2 end,
		priority = 10,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_LARGE,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_MEDLARGE,
		cooktime = 1,
        potlevel = "low",
	},
]]
	bisque =
	{
		test = function(cooker, names, tags) return names.limpets and names.limpets == 3 and tags.frozen end,
		priority = 30,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_HUGE,
		hunger = TUNING.CALORIES_MEDSMALL,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_TINY,
		cooktime = 1,
	},

--[[
	ceviche =
	{
		test = function(cooker, names, tags) return tags.fish and tags.fish >= 2 and tags.frozen end,
		priority = 20,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_MED,
		hunger = TUNING.CALORIES_MED,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_TINY,
		temperature = TUNING.COLD_FOOD_BONUS_TEMP,
		temperatureduration = TUNING.FOOD_TEMP_AVERAGE,
		cooktime = 0.5,
		oneat_desc = STRINGS.UI.COOKBOOK.FOOD_EFFECTS_COLD_FOOD,
	},
]]

	jellyopop =
	{
		test = function(cooker, names, tags) return tags.jellyfish and tags.frozen and names.twigs end,
		priority = 20,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_MED,
		hunger = TUNING.CALORIES_SMALL,
		perishtime = TUNING.PERISH_SUPERFAST,
		sanity = 0,
		temperature = TUNING.COLD_FOOD_BONUS_TEMP,
		temperatureduration = TUNING.FOOD_TEMP_AVERAGE,
		cooktime = 0.5,
        potlevel = "low",
		oneat_desc = STRINGS.UI.COOKBOOK.FOOD_EFFECTS_COLD_FOOD,
	},

--[[
	bananapop =
	{
		test = function(cooker, names, tags) return names.cave_banana and tags.frozen and tags.inedible and not tags.meat and not tags.fish end,
		priority = 20,
		foodtype = FOODTYPE.VEGGIE,
		health = TUNING.HEALING_MED,
		hunger = TUNING.CALORIES_SMALL,
		perishtime = TUNING.PERISH_SUPERFAST,
		sanity = TUNING.SANITY_LARGE,
		temperature = TUNING.COLD_FOOD_BONUS_TEMP,
		temperatureduration = TUNING.FOOD_TEMP_AVERAGE,
		cooktime = 0.5,
	},
]]

	lobsterbisque =
	{
		test = function(cooker, names, tags) return names.lobster and tags.frozen end,
		priority = 30,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_HUGE,
		hunger = TUNING.CALORIES_MED,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_SMALL,
		cooktime = 0.5,
	},

	lobsterdinner =
	{
		test = function(cooker, names, tags) return names.lobster and tags.fat and not tags.meat and not tags.frozen end,
		priority = 25,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_HUGE,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_SLOW,
		sanity = TUNING.SANITY_HUGE,
		cooktime = 1,
	},

	sharkfinsoup =
	{
		test = function(cooker, names, tags) return names.shark_fin end,
		priority = 20,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_LARGE,
		hunger = TUNING.CALORIES_SMALL,
		perishtime = TUNING.PERISH_MED,
		sanity = -TUNING.SANITY_SMALL,
		naughtiness = 10,
		cooktime = 1,
        potlevel = "low",
		oneat_desc = STRINGS.UI.COOKBOOK.FOOD_EFFECTS_NAUGHTY,
	},
--[[
	surfnturf =
	{
		test = function(cooker, names, tags) return tags.meat and tags.meat >= 2.5 and tags.fish and tags.fish >= 1.5 and not tags.frozen end,
		priority = 30,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_HUGE,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_LARGE,
		cooktime = 1,
        potlevel = "high",
	},
]]
	coffee =
	{
		test = function(cooker, names, tags) return names.coffeebeans_cooked and (names.coffeebeans_cooked == 4 or (names.coffeebeans_cooked == 3 and (tags.dairy or tags.sweetener)))	end,
		priority = 30,
		-- foodtype = FOODTYPE.VEGGIE,
		foodtype = FOODTYPE.GOODIES, --Taffy and others got changed to this too
		health = TUNING.HEALING_SMALL,
		hunger = TUNING.CALORIES_TINY,
		perishtime = TUNING.PERISH_MED,
		sanity = -TUNING.SANITY_TINY,
		caffeinedelta = TUNING.CAFFEINE_FOOD_BONUS_SPEED,
		caffeineduration = TUNING.FOOD_SPEED_LONG,
		cooktime = 0.5,
        potlevel = "low",
		oneat_desc = STRINGS.UI.COOKBOOK.FOOD_EFFECTS_SPEED_BOOST,
	},

	tropicalbouillabaisse =
	{
		test = function(cooker, names, tags) return (names.pondpurple_grouper or names.purple_grouper or names.purple_grouper_cooked) and (names.pondpierrot_fish or names.pierrot_fish or names.pierrot_fish_cooked) and (names.pondneon_quattro or names.neon_quattro or names.neon_quattro_cooked) and tags.veggie end,
		priority = 35,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_MED,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_MED,
		cooktime = 2,
        potlevel = "low",
        boost_dry = true,
        boost_cool = true,
        boost_surf = true,
		oneat_desc = STRINGS.UI.COOKBOOK.FOOD_EFFECTS_TROPICAL_BOUILLABAISSE,
	},

	caviar =
	{
		-- test = function(cooker, names, tags) return (names.roe or 0) + (names.roe_cooked or 0) == 3 and tags.veggie end,
		test = function(cooker, names, tags) return (names.roe or names.roe_cooked == 3) and tags.veggie end,
		priority = 20,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_SMALL,
		hunger = TUNING.CALORIES_SMALL,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_LARGE,
		cooktime = 2,
	},
}
local warlyfoods = {
	sweetpotatosouffle =
	{
		test = function(cooker, names, tags) return (names.sweet_potato and names.sweet_potato == 2) and tags.egg and tags.egg >= 2 end,
		priority = 30,
		foodtype = FOODTYPE.VEGGIE,
		health = TUNING.HEALING_MED,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_MED,
		cooktime = 2,
        potlevel = "low",
        tags = { "masterfood" },
	},

--[[
	monstertartare =
	{
		-- test = function(cooker, names, tags) return (names.monstermeat or names.monstermeat_dried or names.cookedmonstermeat)
			-- and tags.monster and tags.monster >= 2 and tags.egg and tags.veggie end,
		test = function(cooker, names, tags) return tags.monster and tags.monster >= 2 and tags.egg and tags.veggie end,
		priority = 30,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_SMALL,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_SMALL,
		cooktime = 2,
        tags = { "masterfood" },
	},
]]

--[[
	freshfruitcrepes =
	{
		test = function(cooker, names, tags) return tags.fruit and tags.fruit >= 1.5 and tags.dairy and names.honey end,
		priority = 30,
		foodtype = FOODTYPE.VEGGIE,
		health = TUNING.HEALING_HUGE,
		hunger = TUNING.CALORIES_SUPERHUGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_MED,
		cooktime = 2,
        tags = { "masterfood" },
	},
]]

	musselbouillabaise =
	{
		test = function(cooker, names, tags) return (names.mussel and names.mussel == 2) and tags.veggie and tags.veggie >= 2 end,
		priority = 30,
		foodtype = FOODTYPE.MEAT,
		health = TUNING.HEALING_MED,
		hunger = TUNING.CALORIES_LARGE,
		perishtime = TUNING.PERISH_MED,
		sanity = TUNING.SANITY_MED,
		cooktime = 2,
        potlevel = "low",
        tags = { "masterfood" },
	},
}

if IA_CONFIG.oldwarly then
	--upgrade monstertartare
	local monstertartare = cooking.GetRecipe("portablecookpot","monstertartare")
	if monstertartare then
		monstertartare.test = function(cooker, names, tags) return tags.monster and tags.monster >= 2 and tags.egg and tags.veggie end
		monstertartare.health = TUNING.HEALING_SMALL
		monstertartare.hunger = TUNING.CALORIES_LARGE
		monstertartare.perishtime = TUNING.PERISH_MED
		monstertartare.sanity = TUNING.SANITY_SMALL
		monstertartare.cooktime = 2
	end
end

----------------------------------------------------------------------------------------

for k,v in pairs(foods) do
	v.name = k
	v.weight = v.weight or 1
	v.priority = v.priority or 0
	IAENV.AddCookerRecipe("cookpot", v)
	-- if not IA_CONFIG.oldwarly then
		IAENV.AddCookerRecipe("portablecookpot", v)
	-- end
end

for k,v in pairs(warlyfoods) do
	v.name = k
	v.weight = v.weight or 1
	v.priority = v.priority or 0
	IAENV.AddCookerRecipe("portablecookpot", v)
end

-- spice it!
local spicedfoods = shallowcopy(require("spicedfoods"))
GenerateSpicedFoods(foods)
GenerateSpicedFoods(warlyfoods)
local ia_spiced = {}
local new_spicedfoods = require("spicedfoods")
for k,v in pairs(new_spicedfoods) do
	if not spicedfoods[k] then
		ia_spiced[k] = v
	end
end
for k,v in pairs(ia_spiced) do
	new_spicedfoods[k] = nil --do not let the game make the prefabs
	IAENV.AddCookerRecipe("portablespicer", v)
end

IA_PREPAREDFOODS = MergeMaps(foods, warlyfoods, ia_spiced)

----------------------------------------------------------------------------------------

--The following makes "portablecookpot" a synonym of "cookpot" and also implements Warly's unique recipes
local CalculateRecipe_old = cooking.CalculateRecipe
cooking.CalculateRecipe = function(cooker, names, ...)
	-- Spicer wetgoop fix! (in the unlikely case somebody has Gourmet food and a spicer at the same time)
	for k, v in pairs(names) do
		if v:sub(-8) == "_gourmet" then
			names[k] = v:sub(1, -9)
		end
	end

	if not IA_CONFIG.oldwarly then return CalculateRecipe_old(cooker, names, ...) end

	if cooker == "portablecookpot" then cooker = "cookpot" end
	local ret
	if cooking.enableWarly and cooker == "cookpot" then
		--TODO This includes meatballs n shit now
		ret = {CalculateRecipe_old("portablecookpot", names, ...)} --get Warly recipe
	end
	if not ret or not ret[1] then
		ret = {CalculateRecipe_old(cooker, names, ...)}
	end
	return unpack(ret)
end

--This can be called when the food is done, thus don't use cooking.enableWarly
local GetRecipe_old = cooking.GetRecipe
cooking.GetRecipe = function(cooker, ...)
	if not IA_CONFIG.oldwarly then return GetRecipe_old(cooker, ...) end

	if cooker == "portablecookpot" then cooker = "cookpot" end
	-- local ret
	-- if cooking.enableWarly and cooker == "cookpot" then
		-- ret = GetRecipe_old("portablecookpot", ...)
	-- end
	-- ret = ret or GetRecipe_old(cooker, ...) or GetRecipe_old("portablecookpot", ...)
	return GetRecipe_old(cooker, ...) or GetRecipe_old("portablecookpot", ...)
end
local IsModCookingProduct_old = IsModCookingProduct
IsModCookingProduct = function(cooker, ...)
	-- if not IA_CONFIG.oldwarly then return IsModCookingProduct_old(cooker, ...) end

	if cooker == "portablecookpot" then cooker = "cookpot" end
	return IsModCookingProduct_old(cooker, ...) or IsModCookingProduct_old("portablecookpot", ...)
end
