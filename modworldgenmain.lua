local require = GLOBAL.require

GLOBAL.SIZE_VARIATION = 4
-- Import dependencies.
local STRINGS = GLOBAL.STRINGS
local iatasks = require "map/levels/ia"
require "map/tasks/volcano"
-- require "map/tasksets/ia"

AddTaskSet("volcanoset", {
	name = "VolcanoSet",
	location = "caves",
	tasks = {
		"volcano",
	},
	numoptionaltasks = 1,
	optionaltasks = {
	},
	valid_start_tasks = {
		"volcano"
	},
	required_prefabs = {
		"obsidian_workbench",
	},
	set_pieces = {
		ObsidianWorkbench = { cout = 1 , tasks = {"volcano"} },
	},
	required_setpieces = {"VolcanoStart"}
})

AddStartLocation("VolcanoDoor", {
	name = STRINGS.UI.SANDBOXMENU.DEFAULTSTART,
	location = "forest",
	start_setpeice = "VolcanoDoor",
	start_node = "VolcanoNoise",
})

AddLevel(GLOBAL.LEVELTYPE.SURVIVAL, {
	id = "VOLCANO_LEVEL",
	name = "Volcano",
	desc = "The top of the SW volcano",
	location = "forest",
	version = 4,
	overrides = {
		task_set = 			"volcanoset",
		start_location = 	"VolcanoDoor",
		world_size = 		"small", --mini in DS
		loop_percent =		"always",
		location =			"forest",
		boons =				"never",
		poi = 				"never",
		traps =				"never",
		protected = 		"never",
		--start_setpeice = 	"MyStaticLayout",
		--start_node =		"VolcanoNoise",

		roads = 			"never",	--路
		frograin = 			"never",	--青蛙雨
		wildfires = 		"never",	--野火
		grassgekkos =		"never",	--草壁虎
		deerclops = 		"never",	--巨鹿
		bearger = 			"never",	--熊大
		perd = 				"never",	--火鸡
		penguins = 			"never",	--企鹅
		hunt = 				"never",	--脚印
		hounds = 			"never",   	--猎犬
		birds = 			"never", 	--鸟
		butterfly = 		"never",  	--蝴蝶
		seagull = 			"never",   	--海鸥
		jellyfish = 		"never",  	--水母
		twister = 			"never",    --豹卷风
		floods = 			"never",	--洪水

		primaryworldtype = "islandsonly",
		isvolcano = "yes"
	},
	required_prefabs = {
		"volcano_altar",
		"obsidian_workbench",
		"volcano_start"
	},
	tasks = {
		"Volcano",
	},
	background_node_range = {0,0},

	required_setpieces = {"ObsidianWorkbench", "VolcanoStart", "VolcanoAltar"}
})

AddLevelPreInitAny(function(level)
	if level.id == "SURVIVAL_SHIPWRECKED_CLASSIC" and GLOBAL.IA_worldtype == "islandsonly" then  --solve when the world size is huge , World chaos
		level.numoptionaltasks = 0
		level.ocean_population = {}
		level.ocean_prefill_setpieces = {}
		level.overrides.task_set = "islandadventures"
		level.required_prefabs = {"multiplayer_portal", "octopusking"}
		level.required_setpieces = {}
		level.set_pieces = 	{
			ResurrectionStoneSw = { count = 2, tasks = {"ShellingOut", "JungleMarsh", GLOBAL.unpack(iatasks[2])} },
			SWPortal = { count = 1, tasks = {GLOBAL.unpack(iatasks[1]), GLOBAL.unpack(iatasks[2])} },
			Airstrike = { count = 1, tasks = {GLOBAL.unpack(iatasks[1]), GLOBAL.unpack(iatasks[2])} },
		}
		level.tasks = {
			"DesertIsland",
			"DoydoyIslandGirl",
			"DoydoyIslandBoy",
			"IslandCasino",
			"PirateBounty",
			"ShellingOut",
			"JungleMarsh",
			"IslandMangroveOxBoon",
			"SharkHome",
			"IslandOasis",
			"HomeIslandSmallBoon_Road",
			"IslandParadise",
			"IslandMeadowBees",
			"JungleSRockyland",
			"Marshy",
			"Cranium",
			-- "VolcanoIsland",
		}
		level.valid_start_tasks = {"HomeIslandSmallBoon_Road"}
	end

	if level.location == "forest" and level.id ~= "SURVIVAL_SHIPWRECKED_CLASSIC" and level.id ~= "VOLCANO_LEVEL" and GetModConfigData("shipwreckedid") then
		level.set_pieces.SunkenBoat = {count = 1, tasks = {"MoonIsland_Beach"}}

		table.insert(level.required_prefabs, "sunken_boat_spawn")
	end
end)

AddLevel(GLOBAL.LEVELTYPE.SURVIVAL, {
	id = "SURVIVAL_SHIPWRECKED_CLASSIC",
	name = STRINGS.UI.CUSTOMIZATIONSCREEN.PRESETLEVELS.SURVIVAL_SHIPWRECKED_CLASSIC,
	desc = STRINGS.UI.CUSTOMIZATIONSCREEN.PRESETLEVELDESC.SURVIVAL_SHIPWRECKED_CLASSIC,
	location = "forest",
	version = 4,
	overrides = {
		task_set = "islandadventures",
		start_location = "islandadventures",
		prefabswaps_start = "classic",
		loop = "always", --"never" in SW, but here it helps keep things square-ish to fit the map better. -M
		branching = "most",
		roads = "never",
		frograin = "never",
		wildfires = "never",

		deerclops = "never",
		bearger = "never",
		-- goosemoose = "never", --unnecessary
		-- dragonfly = "never", --unnecessary
		-- antliontribute = "never", --unnecessary

		perd = "never",
		penguins = "never",
		hunt = "never",

		primaryworldtype = "islandsonly",
	},
	required_prefabs = {"octopusking"},
	required_setpieces = {
		-- "Sculptures_1",
		-- "Maxwell5",
	},
	-- numrandom_set_pieces = 4,
	numrandom_set_pieces = 0,
	random_set_pieces =
	{
		-- "Sculptures_2",
		-- "Sculptures_3",
		-- "Sculptures_4",
		-- "Sculptures_5",
		-- "Chessy_1",
		-- "Chessy_2",
		-- "Chessy_3",
		-- "Chessy_4",
		-- "Chessy_5",
		-- "Chessy_6",
		-- --"ChessSpot1",
		-- --"ChessSpot2",
		-- --"ChessSpot3",
		-- "Maxwell1",
		-- "Maxwell2",
		-- "Maxwell3",
		-- "Maxwell4",
		-- "Maxwell6",
		-- "Maxwell7",
		-- "Warzone_1",
		-- "Warzone_2",
		-- "Warzone_3",
	},
})

AddStartLocation("islandadventures", {
    name = STRINGS.UI.SANDBOXMENU.IA_START,
    location = "forest",
    start_setpeice = "IA_Start",
    start_node = "BeachSandHome_Spawn",
})

AddTaskSet("islandadventures", {
	name = STRINGS.UI.CUSTOMIZATIONSCREEN.TASKSETNAMES.ISLANDADVENTURES,
	location = "forest",
	tasks = {
		"DesertIsland",
		"DoydoyIslandGirl",
		"DoydoyIslandBoy",
		"IslandCasino",
		"PirateBounty",
		"ShellingOut",
		"JungleMarsh",
		"IslandMangroveOxBoon",
		"SharkHome",
		"IslandOasis",
		"HomeIslandSmallBoon_Road",
		"IslandParadise",
		"IslandMeadowBees",
		"JungleSRockyland",
		"Marshy",
		"Cranium",
		-- "VolcanoIsland",
	},
	numoptionaltasks = 0, --22,
	optionaltasks = {
		"BeachBothJungles",
		"BeachJingleS",
		"BeachSavanna",
		"GreentipA",
		"GreentipB",
		"HalfGreen",
		"BeachRockyland",
		"LotsaGrass",
		"CrashZone",
		"BeachJungleD",
		"AllBeige",
		"BeachMarsh",
		"Verdant",
		"Vert",
		"VerdantMost",
		"Florida Timeshare",
		"PiggyParadise",
		"BeachPalmForest",
		"IslandJungleShroomin",
		"IslandJungleNoFlowers",
		"IslandBeachGrassy",
		"IslandBeachRocky",
		"IslandBeachSpider",
		"IslandBeachNoCrabbits",
		"JungleSSavanna",
		"JungleBeige",
		"Spiderland",
		"IslandJungleBamboozled",
		"IslandJungleNoBerry",
		"IslandBeachDunes",
		"IslandBeachSappy",
		"IslandBeachNoLimpets",
		"JungleDense",
		"JungleDMarsh",
		"JungleDRockyland",
		"JungleDRockyMarsh",
		"JungleDSavanna",
		"JungleDSavRock",
		"ThemeMarshCity",
		"IslandJungleCritterCrunch",
		"IslandRockyTallJungle",
		"IslandBeachNoFlowers",
		"NoGreen A",
		"KelpForest",
		"GreatShoal",
		"BarrierReef",
		"HotNSticky",
		"Rockyland",
		"IslandJungleMonkeyHell",
		"IslandJungleSkeleton",
		"FullofBees",
		"IslandJungleRockyDrop",
		"IslandJungleEvilFlowers",
		"IslandBeachCrabTown",
		"IslandBeachForest",
		"IslandJungleNoRock",
		"IslandJungleNoMushroom",
		"NoGreen B",
		"Savanna",
		"IslandBeachLimpety",
		"IslandRockyGold",
		"IslandRockyTallBeach",
		"IslandMeadowCarroty",
	},
	valid_start_tasks = {
		"HomeIslandSmallBoon_Road",
	},
	set_pieces = {
		-- ResurrectionStone = { count=2, tasks={"Make a pick", "Dig that rock", "Great Plains", "Squeltch", "Beeeees!", "Speak to the king", "Forest hunters" } },
		-- ResurrectionStoneSw = { count=2, tasks={ "IslandParadise", "VerdantMost", "AllBeige", "NoGreen B", "Florida Timeshare","PiggyParadise","JungleDRockyland","JungleDRockyMarsh","JungleDSavRock","IslandJungleRockyDrop", } },
		ResurrectionStoneSw = { count = 2, tasks = {"ShellingOut", "JungleMarsh", GLOBAL.unpack(iatasks[2])} },
		SWPortal = { count = 1, tasks = {GLOBAL.unpack(iatasks[1]), GLOBAL.unpack(iatasks[2])} },
		Airstrike = { count = 1, tasks = {GLOBAL.unpack(iatasks[1]), GLOBAL.unpack(iatasks[2])} },
		-- WormholeGrass = { count=8, tasks={"Make a pick", "Dig that rock", "Great Plains", "Squeltch", "Beeeees!", "Speak to the king", "Forest hunters", "Befriend the pigs", "For a nice walk", "Kill the spiders", "Killer bees!", "Make a Beehat", "The hunters", "Magic meadow", "Frogs and bugs" } },
		-- CaveEntrance = { count = 10, tasks={"Make a pick", "Dig that rock", "Great Plains", "Squeltch", "Beeeees!", "Speak to the king classic", "Forest hunters", "Befriend the pigs", "For a nice walk", "Kill the spiders", "Killer bees!", "Make a Beehat", "The hunters", "Magic meadow", "Frogs and bugs"} },
	},
})
