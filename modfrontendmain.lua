OnUnloadMod = function()
    local servercreationscreen --= GLOBAL.TheFrontEnd:GetActiveScreen()
    for _, screen in pairs(GLOBAL.TheFrontEnd.screenstack) do
        if screen.name == "ServerCreationScreen" then
            servercreationscreen = screen
            break
        end
    end

    local forest_tab = servercreationscreen and servercreationscreen.world_tabs and servercreationscreen.world_tabs[1]
    local cave_tab = servercreationscreen and servercreationscreen.world_tabs and servercreationscreen.world_tabs[2]
    if forest_tab then
        for _, v in ipairs(forest_tab.worldsettings_widgets) do
            v:OnPresetButton()
        end
    end

    if cave_tab then
        for _, v in ipairs(cave_tab.worldsettings_widgets) do
            v:OnPresetButton()
        end
    end
end
