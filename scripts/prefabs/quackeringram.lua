local MakeVisualBoatEquip = require("prefabs/visualboatequip")
local easing = require "easing"

local assets=
{
    Asset("ANIM", "anim/swap_quackeringram.zip"),
}

local prefabs =
{
    "quackering_wave"
}

-- from PIL: utility to check for item in list
local function Set(list)
	local set = {}
	for _, l in ipairs(list) do set[l] = true end
	return set
end

local function onembarked(boat, data)
	local item = boat.components.container:GetItemInBoatSlot(BOATEQUIPSLOTS.BOAT_LAMP)
	if item and item.visual then
		item.visual.AnimState:OverrideSymbol("swap_lantern", "swap_quackeringram", "swap_quackeringram")
	end
end

local function ondisembarked(boat, data)
	local item = boat.components.container:GetItemInBoatSlot(BOATEQUIPSLOTS.BOAT_LAMP)
	if item and item.visual then
		--item.visual.AnimState:ClearOverrideSymbol("swap_lantern")
		item:change_quackeringwave(data.sailor, "hide")
		item.SoundEmitter:KillSound("ram_LP")
	end
end

local function onequip(inst, owner)
    --print("equip, override on owner " ..tostring(owner))
    if owner.components.boatvisualmanager then
        owner.components.boatvisualmanager:SpawnBoatEquipVisuals(inst, inst.prefab)
    end

    if inst.visual then
        inst.visual.AnimState:OverrideSymbol("swap_lantern", "swap_quackeringram", "swap_quackeringram")
    end

    inst.equippedby = owner

    --print("Listening for mount/dismount from " ..tostring(owner))
    inst:ListenForEvent("embarked", onembarked, owner)
    inst:ListenForEvent("disembarked", ondisembarked, owner)
end

local function onunequip(inst, owner)
    if owner.components.boatvisualmanager then
        owner.components.boatvisualmanager:RemoveBoatEquipVisuals(inst)
    end
    --print("onunequip, clear on owner " ..tostring(owner))
    if inst.visual then
        inst.visual.AnimState:ClearOverrideSymbol("swap_lantern")
    end

	local sailor =  owner.components.sailable and owner.components.sailable.sailor
	if sailor then
		inst:change_quackeringwave(sailor, "hide")
    end

    inst.equippedby = nil

    --print("Removing EventCallbacks for mount/dismount from " ..tostring(owner))
    inst:RemoveEventCallback("embarked", onembarked, owner)
    inst:RemoveEventCallback("disembarked", ondisembarked, owner)
end

local function onfinished(inst)
    if inst.equippedby then
        inst.equippedby.AnimState:ClearOverrideSymbol("swap_lantern")

		local sailor = inst.equippedby.components.sailable and inst.equippedby.components.sailable.sailor
        if sailor then
			sailor.AnimState:ClearOverrideSymbol("swap_lantern")
			inst:change_quackeringwave(sailor, "hide")

			if sailor.wakeTask then
				print("clearing wake task")
				sailor.wakeTask:Cancel()
				sailor.wakeTask = nil
			end
        end
    end

	inst:Remove()
end

local function spawnWake(driver)
	local wake = SpawnPrefab("quackering_wake")
	if driver.wakeLeft == true then
		wake.idleanimation = "idle"
		driver.wakeLeft = false
	else
		wake.idleanimation = "idle_2"
		driver.wakeLeft = true
	end
	local x, y, z = driver.Transform:GetWorldPosition()
    wake.Transform:SetPosition( x, y, z )
    wake.Transform:SetRotation(driver.Transform:GetRotation())
	--print("Spawning wake at:"..x..","..z)

	if driver.wakeTask then
		driver.wakeTask:Cancel()
		driver.wakeTask = nil
	end

	driver.wakeTask = driver:DoTaskInTime(5 * FRAMES, function() spawnWake(driver) end)
end

local function onPotentialRamHit(inst, target)

	local function performRamFX(inst, target)
		local quackeringRamFX = "boat_hit_fx_quackeringram"
		for i=1, 5, 1 do
			local impactFX = SpawnPrefab(quackeringRamFX)

			local dx = math.random(-3, 3)
			local dz = math.random(-3, 3)

			local x, y, z = target.Transform:GetWorldPosition()

			impactFX.Transform:SetPosition(x + dx, y, z + dz)
		end

        local driver = inst.components.rammer:FindDriver()
		if driver then
			local currentSpeed = driver.Physics:GetMotorSpeed()
			local boost = 25

			inst.SoundEmitter:PlaySound("ia/common/quackering_ram/impact")
			driver.Physics:SetMotorVel(currentSpeed + boost, 0, 0)
			driver.wakeLeft = true
			driver.wakeTask = spawnWake(driver)
			driver:DoTaskInTime(40 * FRAMES, function()
				if driver and driver.wakeTask then
					driver.wakeTask:Cancel()
					driver.wakeTask = nil
				end
			end)

			-- minor camera shake on hit
			-- function PlayerController:ShakeCamera(inst, shakeType, duration, speed, maxShake, maxDist)
			local distSq = distsq(driver:GetPosition(), target:GetPosition())
			local maxShake, maxDist = 0.35, 40
			local t = math.max(0, math.min(1, distSq / (maxDist * maxDist)))
			local scale = easing.outQuad(t, maxShake, -maxShake, 1)
			if scale > 0 then
				driver:ShakeCamera(CAMERASHAKE.VERTICAL, 0.2, 0.025, scale)
			end
		end
	end

	local hitTarget = false
    local inpocket = target.components.inventoryitem and target.components.inventoryitem:IsHeld()
    local driver = inst.components.rammer:FindDriver()
    local isfriend = (target == driver) or target:HasTag("companion") or (target.components.follower and target.components.follower.leader == driver)

	local IsInsulated_old
	if driver and driver.components.inventory then
		IsInsulated_old = driver.components.inventory.IsInsulated
		function driver.components.inventory:IsInsulated()
			return true  --在撞击时不能被电，否则有BUG (It cannot be electrified during impact, otherwise there will be a bug) -k
		end
	end

    if not inpocket and not isfriend then
		if target.components.sailable and target.components.sailable.sailor then --当玩家在船上时不能毁掉船（when player in boat, don't destroy boat） -K
		elseif target.components.combat and not target:HasTag("playerghost") and ( TheNet:GetServerGameMode() == "wilderness" or not target:HasTag("player") ) then --荒野模式可以伤害玩家(open wilderness mode can attack player) -k
            hitTarget = true
            if driver then
				if driver.components.inventory then
					driver.components.inventory.insulated = true
				end

	            target.components.combat:GetAttacked(driver, TUNING.QUACKERINGRAM_DAMAGE, inst)

				if driver.components.inventory then
					driver.components.inventory.insulated = false
				end
			end
        elseif target.components.workable and not target:HasTag("busy") then --Haaaaaaack!
            hitTarget = true
            target.components.workable:Destroy(inst)
        end
    end

	-- common tasks when hitting a target
	if hitTarget then
		-- show fx
		performRamFX(inst, target)

		-- use up a charge
        inst.components.finiteuses:Use()

		-- cooldown, avoid double hits
		inst.components.rammer:StartCooldown()
	end

	if IsInsulated_old and driver and driver.components.inventory then
    	driver.components.inventory.IsInsulated = IsInsulated_old --还原(reduction) -k
	end

end

local function fn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
	inst.entity:AddNetwork()
    inst.Transform:SetFourFaced()
    inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()

    -- used for collision checks in boat.lua
    inst:AddTag("quackeringram")

    inst.AnimState:SetBuild("swap_quackeringram")
    inst.AnimState:SetBank("quackeringram")
    inst.AnimState:PlayAnimation("idle")

    MakeInventoryPhysics(inst)
    MakeInventoryFloatable(inst)
	inst.components.floater:UpdateAnimations("idle_water", "idle")

	function inst:change_quackeringwave(player, change)
		local player_classified = player.player_classified
		player_classified.quackeringwavechange:set(change)
	end

	inst.entity:SetPristine()

	if not TheWorld.ismastersim then
		return inst
	end

    inst:AddComponent("inspectable")
    inst:AddComponent("inventoryitem")

    inst:AddComponent("equippable")
    inst.components.equippable.boatequipslot = BOATEQUIPSLOTS.BOAT_LAMP
    inst.components.equippable.equipslot = nil
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)
	inst.components.equippable.insulated = true

    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.QUACKERINGRAM_USE_COUNT)
    inst.components.finiteuses:SetUses(TUNING.QUACKERINGRAM_USE_COUNT)
    inst.components.finiteuses:SetOnFinished(onfinished)

	--inst.windFX = SpawnPrefab("quackering_wave")
	--inst.windFX:HideEffect()
	inst.SoundEmitter:KillSound("ram_LP")

	inst:AddComponent("rammer")
	inst.components.rammer.minSpeed = 2.5
	inst.components.rammer.onRamTarget = onPotentialRamHit

	inst.components.rammer.onUpdate = function(dt)
		local driver = inst.components.rammer:FindDriver()
        if inst.components.rammer:IsActive() and driver then
			inst:change_quackeringwave(driver, "show")
        end
	end

	inst.components.rammer.onActivate = function()
		inst.SoundEmitter:PlaySound("ia/common/quackering_ram/impact")
		inst.SoundEmitter:PlaySound("ia/common/quackering_ram/ram_LP", "ram_LP")

		local driver = inst.components.rammer:FindDriver()
		local currentSpeed = driver.Physics:GetMotorSpeed()
		local boost = 8
		driver.Physics:SetMotorVel(currentSpeed + boost, 0, 0)
	end

	inst.components.rammer.onDeactivate = function()
		local driver = inst.components.rammer:FindDriver()
		if driver then
			inst:change_quackeringwave(driver, "hide")
		end

		inst.SoundEmitter:KillSound("ram_LP")
	end

    return inst
end

local function quackeringram_visual_common(inst)
    inst.AnimState:SetBank("sail_visual")
    inst.AnimState:SetBuild("swap_quackeringram")
    inst.AnimState:PlayAnimation("idle_loop")
    inst.AnimState:SetSortWorldOffset(0, 0.05, 0) --below the player

    function inst.components.boatvisualanims.update(inst, dt)
        if inst.AnimState:GetCurrentFacing() == FACING_DOWN then
            inst.AnimState:SetSortWorldOffset(0, 0.15, 0) --above the player
        else
            inst.AnimState:SetSortWorldOffset(0, 0.05, 0) --below the player
        end
    end
end

return Prefab("quackeringram", fn, assets, prefabs),
    MakeVisualBoatEquip("quackeringram", assets, nil, quackeringram_visual_common)
