local assets =
{
    Asset("ANIM", "anim/messagebottle.zip"),
}

local function clientcommonfn()
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)
    MakeInventoryFloatable(inst)

    inst.AnimState:SetBank("messagebottle")
    inst.AnimState:SetBuild("messagebottle")

    inst.entity:SetPristine()
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("inspectable")

    MakeInvItemIA(inst)

    inst:AddComponent("waterproofer")
    inst.components.waterproofer:SetEffectiveness(0)

    inst.no_wet_prefix = true

    return inst
end

local function onreadmap(inst, doer)
    if (not inst.treasure and inst.treasureguid) or TheWorld:HasTag("volcano") or not TheWorld:HasTag("island") then
        doer.components.talker:Say( GetString(doer, "ANNOUNCE_OTHER_WORLD_TREASURE") )
        return
    end

    local buriedtreasure = nil
    if #AllBuriedtreasure > 0 then
        local order = math.random(#AllBuriedtreasure)
        if not AllBuriedtreasure[order].revealed then
            buriedtreasure = AllBuriedtreasure[order]
        end
    end

    if not buriedtreasure then
        local x, y
        local map = TheWorld.Map
        local sx, sy = map:GetSize()
        local i = 1
        repeat
            x = math.random(-sx, sx)
            y = math.random(-sy, sy)
            local tile = map:GetTileAtPoint(x, 0, y)
            i = i + 1
        until not IsWater(tile) or i == 1000
        buriedtreasure = SpawnPrefab("buriedtreasure")
        buriedtreasure.Transform:SetPosition(x, 0, y)
    end
    local pos = buriedtreasure:GetPosition()
    buriedtreasure:SetRandomNewTreasure()
    inst:Remove()

    TheWorld.components.timer:StartTimer(string.format("%d", buriedtreasure.GUID) .. "ia_messagebottle", TUNING.TOTAL_DAY_TIME * 10) -- new bottle

    doer:DoTaskInTime(3 * FRAMES, function()
        doer.components.inventory:GiveItem( SpawnPrefab("ia_messagebottleempty") )
    end)

    return pos
end


local function messagebottlefn()
    local inst = clientcommonfn()
    inst.entity:AddMiniMapEntity()
    inst.MiniMapEntity:SetIcon("ia_messagebottle.tex")

	inst:AddTag("ia_messagebottle")
	inst:AddTag("nosteal")
	inst:AddTag("aquatic")
    inst:AddTag("scroll")

	inst.components.floater:UpdateAnimations("idle_water", "idle")

    if not TheWorld.ismastersim then
        return inst
    end

	--This won't do, only Wickerbottom and Maxwell can read.
	--inst:AddComponent("book")
	--inst.components.book.onread = readfn

    inst:AddComponent("mapspotrevealer")
	inst.components.mapspotrevealer:SetGetTargetFn(onreadmap)

  return inst
end

local function emptybottlefn()
    local inst = clientcommonfn()

	inst.components.floater:UpdateAnimations("idle_water_empty", "idle_empty")

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = TUNING.STACK_SIZE_MEDITEM

  return inst
end

return Prefab("ia_messagebottle", messagebottlefn, assets),
       Prefab("ia_messagebottleempty", emptybottlefn, assets)
