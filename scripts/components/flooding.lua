--Flood tiles are fake water on land tiles, and they're only half as wide as real tiles.
--In SW, it is mostly handled engine-side.
--Flood exists as tides and as Green season puddles.
--The puddle sources are also known as "puddle eyes", but that name is too silly for me to write serious code with it. -M
--They spread flood tiles around themselves, but can be blocked by sandbags.
--There's an implicit bug that prevents the spread if the source tile is blocked directly.
--Sandbags can also be used to entirely remove the flood, but the details are weird and possibly inconsistent.
--Puddles dry up over the course of three days in summer.

--https://www.lua.org/pil/17.1.html
local GetFloodPoint
local __floodpoint = {
    __add = function(lhs, rhs)
        return GetFloodPoint(lhs.x + rhs.x, lhs.y + rhs.y)
    end,
    __sub = function(lhs, rhs)
        return GetFloodPoint(lhs.x - rhs.x, lhs.y - rhs.y)
    end,
    __unm = function(pt)
        return GetFloodPoint(pt.x == 0 and pt.x or -pt.x, pt.y == 0 and pt.y or -pt.y)
    end,
    __mul = function(lhs, rhs)
        return GetFloodPoint(lhs.x * rhs, lhs.y * rhs)
    end,
    __div = function(lhs, rhs)
        return GetFloodPoint(lhs.x / rhs, lhs.y / rhs)
    end,
    __tostring = function(pt)
        return string.format("(%2.2f, %2.2f)", pt.x, pt.y)
    end
}
local _flood_points = setmetatable({}, {__mode = "v"})
function GetFloodPoint(x, y)
    if not y then
        x, y = x.x, x.y
    end
    local key = tostring(x)..tostring(y)
    if _flood_points[key] then return _flood_points[key] end
    local pt = setmetatable({x = x, y = y}, __floodpoint)
    _flood_points[key] = pt
    return pt
end

local function MakePtTable()
    local table_radius = setmetatable({size = 0}, {
        __index = function(t, k)
            if type(k) == "table" and k.radius then
                local v = rawget(t, k.radius)
                if v ~= nil then return v end
                k = k.radius
            end
            rawset(t, k, setmetatable({size = 0}, {
                __pairs = function(t)
                    return next, t, nil
                end,
                __next = function(t, k)
                    local v
                    repeat
                        k, v = rawnext(t, k)
                    until k == nil or type(k) == "table"
                    return k, v
                end
            }))
            return rawget(t, k)
        end,
    })
    local table = setmetatable({_ = {}}, {
        __index = function(t, k)
            return rawget(t, "_")[k]
        end,
        __newindex = function(t, k, v)
            local _radius = rawget(t, "_")[k]
            if _radius then
                table_radius.size = table_radius.size - 1
                table_radius[_radius].size = table_radius[_radius].size - 1
                table_radius[_radius][k] = nil
            end
            local radius = type(v) == "table" and v.radius or v
            if radius then
                table_radius.size = table_radius.size + 1
                table_radius[radius].size = table_radius[radius].size + 1
                table_radius[radius][k] = true
            end
            rawget(t, "_")[k] = v
        end,
        __pairs = function(t)
            return rawpairs(rawget(t, "_"))
        end,
        __next = function(t, k)
            return rawnext(rawget(t, "_"), k)
        end
    })
    return table, table_radius
end

--------------------------------------------------------------------------

return Class(function(self, inst)

--------------------------------------------------------------------------
--[[ Constants ]]
--------------------------------------------------------------------------

local _moontideheights = {
	["new"] = 0,
    ["quarter"] = 5,
    ["half"] = 7,
    ["threequarter"] = 8,
    ["full"] = 10,
}

local _surrounding_offsets = {
    GetFloodPoint(.5,  0),
    GetFloodPoint(0,   .5),
    GetFloodPoint(-.5, 0),
    GetFloodPoint(0,   -.5),
}

local _inverted_surrounding_offsets = table.invert(_surrounding_offsets)

local _surrounding_vertical_offsets = {
    GetFloodPoint(0, .5),
    GetFloodPoint(0, -.5),
}

local _inverted_surrounding_vertical_offsets = table.invert(_surrounding_vertical_offsets)

local _surrounding_horizontal_offsets = {
    GetFloodPoint(.5,  0),
    GetFloodPoint(-.5, 0),
}

local _inverted_surrounding_horizontal_offsets = table.invert(_surrounding_horizontal_offsets)


--------------------------------------------------------------------------
--[[ Member variables ]]
--------------------------------------------------------------------------
--Public

self.inst = inst

--Private

local _world = TheWorld
local _map = _world.Map
local _ismastersim = _world.ismastersim

local w, h = _map:GetSize()

local _israining = false
local _isfloodseason = false
local _seasonprogress = 0
local _clockprogress = 0

local _maxTide = 0
local _maxTideMod = 1 --settings modifier

local _targetTide = 0
local _currentTide = 0

local _puddles = _ismastersim and {} --array of the puddles
local _puddle_lookup = _ismastersim and {}
local _blocker_lookup = _ismastersim and {} --will probably be needed clientside when tides get added
local _flood_lookup = {}

local _flood_data = {} --temporary storage for use inbetween having queued a flood to spawn, and it actually spawning
local _flood_queue = {} --queue of floods to (eventually) spawn

local _worldfloodprogress = 0
local _floodedworld = false

local _targetPuddleHeight = _ismastersim and 0

-- for puddles
local _maxFloodLevel = _ismastersim and TUNING.MAX_FLOOD_LEVEL
local _forcedFloodLevel = nil
local _timeBetweenFloodIncreases = _ismastersim and TUNING.FLOOD_GROW_TIME
local _timeBetweenFloodDecreases = _ismastersim and TUNING.FLOOD_DRY_TIME
local _spawnerFrequency = _ismastersim and TUNING.FLOOD_FREQUENCY

--------------------------------------------------------------------------
--[[ Private member functions ]]
--------------------------------------------------------------------------

local networktiledata = {}

local function createnetwork(x, y)
    local inst = SpawnPrefab("network_flood")

    inst.Transform:SetPosition((x - w/2) * TILE_SCALE, 0, (y - h/2) * TILE_SCALE)

    return inst
end

local function GetNetworkTileState(x, y)
    if networktiledata[x] then
        return networktiledata[x][y]
    end
end

local function SetNetworkTileState(x, y, val)
    if val then
        --create tile entry if needed
        networktiledata[x] = networktiledata[x] or {}
        networktiledata[x][y] = networktiledata[x][y] or {}
        networktiledata[x][y].val = val
        if not networktiledata[x][y].inst then
            networktiledata[x][y].inst = createnetwork(x, y)
        end
        return networktiledata[x][y]
    elseif networktiledata[x] and networktiledata[x][y] then
        if networktiledata[x][y].inst then
            networktiledata[x][y].val = nil
            networktiledata[x][y].inst:Remove()
        end
    end

end

local function RealToFloodPos(x, y, z)
    --gets the flood position for any entity position.
    x = math.floor(x)
    y = math.floor(z or y)
    return GetFloodPoint((x + ((x+1)%2)) / TILE_SCALE + w / 2, (y + ((y+1)%2)) / TILE_SCALE + h / 2)
end

local function FloodToRealPos(pt)
    --gets the real position for any flood position.
   return Vector3((pt.x - w/2) * TILE_SCALE, 0, (pt.y - h/2) * TILE_SCALE)
end

local function FloodTileExists(pt)
    local tile = GetNetworkTileState(pt.x, pt.y)
    return tile and tile.val == true and tile.flood == true or false
end

local function GetFloodData(pt)
    if FloodTileExists(pt) then
        return GetNetworkTileState(pt.x, pt.y).data
    elseif _flood_queue[pt] then
        _flood_data[pt] = _flood_data[pt] or {}
        return _flood_data[pt]
    end
end

local function ApplyFloodTileState(pt, val)
    val = val and true or false
    if FloodTileExists(pt) == val then return end

    local tilestate = SetNetworkTileState(pt.x, pt.y, val) or {}

    tilestate.data = val and (_flood_data[pt] or {}) or nil
    tilestate.flood = val or nil
    _flood_queue[pt] = nil
    _flood_data[pt] = nil
end

local function QueueFloodTileState(pt, val, source)
    if val then
        _flood_queue[pt] = true
        local tiledata = GetFloodData(pt)
        tiledata.sources = tiledata.sources or {}
        tiledata.sources[source] = true
    else
        local tiledata = GetFloodData(pt)
        if tiledata then
            tiledata.sources = tiledata.sources or {}
            tiledata.sources[source] = nil
            if IsTableEmpty(tiledata.sources) then
                _flood_queue[pt] = false
            end
        end
    end
end

local IsValidSpotForFlood = _ismastersim and function(pt, spawning)
    local pos = FloodToRealPos(pt)
    local ground = _map:GetTileAtPoint(pos:Get())
    return not _blocker_lookup[pt] and IsLand(ground) and ground ~= GROUND.IMPASSABLE and
        (spawning ~= true or (not GROUND_FLOORING[ground] and IsInIAClimate(pos)))
end

local RemovePuddle = _ismastersim and function(puddle)
    table.removearrayvalue(_puddles, puddle)
    _puddle_lookup[puddle.pos] = nil
end

local Puddle = Class(function(self)
    self.pos = nil
    --actually spawned in floods
    self.flooded_pt, self.flooded_radius = MakePtTable()
    --list of floods that are visually valid to spawn
    self.spawn_queue, self.spawn_queue_count = {}, 0
    --list of floods that are visually valid to despawn
    self.despawn_queue, self.despawn_queue_count = {}, 0
    --all the tiles that should contain floods
    self.floodable_pt, self.floodable_radius = MakePtTable()

    self.simple_flood_expansion, self.advanced_flood_expansion = {}, {}

    self.surrounding_flooded_count = setmetatable({}, {__index = function(t, k) rawset(t, k, 0) return rawget(t, k) end})

    self.deadpuddle = false
    self.targetradius = 0
    self.radius = 0
    self.timeSinceIncrease = 0
    self.timeSinceDecrease = 0
end, nil, {})

function Puddle:CanQueueFloodSpawn(pt)
    if not self.floodable_pt[pt] or self.flooded_pt[pt] then return false end
    local surrounding_flooded_count = self.surrounding_flooded_count[pt]
    return self.floodable_pt[pt].radius <= self.targetradius and
        ((surrounding_flooded_count ==  0 and pt == self.pos) or (surrounding_flooded_count >= 1 and self.floodable_pt[pt].s) or surrounding_flooded_count >= 2)
end

function Puddle:CanQueueFloodDespawn(pt)
    if not self.flooded_pt[pt] then return false end
    local surrounding_flooded_count = self.surrounding_flooded_count[pt]
    return self.flooded_pt[pt] > self.targetradius and (surrounding_flooded_count <= 2 and pt ~= self.pos or surrounding_flooded_count == 0)
end

function Puddle:SpawnFlood(pt, data)
    self.flooded_pt[pt] = data
    if not self.flooded_pt[pt] then return end
    self:DequeueFloodSpawn(pt)
    for i, offset in ipairs(_surrounding_offsets) do
        local _pt = pt + offset
        self.surrounding_flooded_count[_pt] = self.surrounding_flooded_count[_pt] + 1
        if not self.spawn_queue[_pt] and self:CanQueueFloodSpawn(_pt) then
            self:QueueFloodSpawn(_pt, self.floodable_pt[_pt].radius)
        end
        if self.despawn_queue[_pt] and not self:CanQueueFloodDespawn(_pt) then
            self:DequeueFloodDespawn(_pt)
        end
    end
end

function Puddle:DespawnFlood(pt)
    if pt == self.pos then
        self.deadpuddle = true
    end
    if not self.flooded_pt[pt] then return end
    self.flooded_pt[pt] = nil
    self:DequeueFloodSpawn(pt)
    self:DequeueFloodDespawn(pt)
    for i, offset in ipairs(_surrounding_offsets) do
        local _pt = pt + offset
        self.surrounding_flooded_count[_pt] = self.surrounding_flooded_count[_pt] - 1
        if self.spawn_queue[_pt] and not self:CanQueueFloodSpawn(_pt) then
            self:DequeueFloodSpawn(_pt)
        end
        if not self.despawn_queue[_pt] and self:CanQueueFloodDespawn(_pt) then
            self:QueueFloodDespawn(_pt, self.flooded_pt[_pt])
        end
    end
end

function Puddle:QueueFloodSpawn(pt, data)
    if self.spawn_queue[pt] then
        self.spawn_queue_count = self.spawn_queue_count - 1
    end
    self.spawn_queue[pt] = data
    if data then
        self.spawn_queue_count = self.spawn_queue_count + 1
    end
end

function Puddle:DequeueFloodSpawn(pt)
    if self.spawn_queue[pt] then
        self.spawn_queue_count = self.spawn_queue_count - 1
    end
    self.spawn_queue[pt] = nil
end

function Puddle:QueueFloodDespawn(pt, data)
    if self.despawn_queue[pt] then
        self.despawn_queue_count = self.despawn_queue_count - 1
    end
    self.despawn_queue[pt] = data
    if data then
        self.despawn_queue_count = self.despawn_queue_count + 1
    end
end

function Puddle:DequeueFloodDespawn(pt)
    if self.despawn_queue[pt] then
        self.despawn_queue_count = self.despawn_queue_count - 1
    end
    self.despawn_queue[pt] = nil
end

function Puddle:SetRadius(radius)
    self.radius = math.max(0, radius or 0)
    return self.radius
end

function Puddle:SetTargetRadius(radius)
    self.targetradius = math.max(0, radius == 1 and 0 or radius or 0) --radius 1 and 0 are the same in SW
    return self.targetradius
end

function Puddle:GetRadius()
    return self.radius
end

function Puddle:GetTargetRadius()
    return self.targetradius
end

function Puddle:GetSourceFlood()
    if self.flooded_pt[self.pos] then
        return self.pos
    end
    return nil --source flood was deleted.
end

function Puddle:UpdateFloodable(radius)
    for i = radius, #self.floodable_radius do
        for k, v in pairs(self.floodable_radius[i]) do
            self.floodable_pt[k] = nil
        end
        self.floodable_radius[i] = nil
    end

    self:SetRadius(#self.floodable_radius)
    self.simple_flood_expansion = {}
    self.advanced_flood_expansion = {}
    for pt, _ in pairs(self.floodable_radius[#self.floodable_radius]) do
        local data = self.floodable_pt[pt]
        if data.a then
            self.advanced_flood_expansion[pt] = _surrounding_offsets[data.d] or false
        else
            self.simple_flood_expansion[pt] = _surrounding_offsets[data.d]
        end
    end
end

--a = advanced, d = direction, s means it can spawn while only touching a single flood(normally requires two floods).
local function AddSimpleFlood(floodable_pt, expansion, radius, pt, dir)
    if not floodable_pt[pt] or radius <= floodable_pt[pt].radius then
        floodable_pt[pt] = {radius = radius, a = false, d = _inverted_surrounding_offsets[dir]}
        expansion[pt] = dir
    end
end

local function AddAdvancedFlood(floodable_pt, expansion, radius, pt, dir, single)
    if not floodable_pt[pt] or radius <= floodable_pt[pt].radius then
        floodable_pt[pt] = {radius = radius, a = true, d = _inverted_surrounding_offsets[dir], s = single}
        expansion[pt] = dir or false
    end
end

local function IsFloodAlone(floodable_pt, newpt, sideoffsets, frontoffset)
    for i, offset in ipairs(sideoffsets) do
        if floodable_pt[newpt + offset] then
            return false
        end
    end
    return not floodable_pt[newpt + frontoffset]
end

function Puddle:DoFloodExpansion()
    if #self.floodable_radius < self:GetTargetRadius() and not _blocker_lookup[self.pos] then  -- let blocked flood no expansion  -Jerry
        local radius = #self.floodable_radius + 1
        if radius == 1 then
            self.simple_flood_expansion = {}
            self.advanced_flood_expansion = {}
            if IsValidSpotForFlood(self.pos) then
                AddAdvancedFlood(self.floodable_pt, self.advanced_flood_expansion, radius, self.pos, nil)
            end
            return
        end

        local new_simple_flood_expansion = {}
        local new_advanced_flood_expansion = {}

        local flood_flow_blocked, flood_test_alone = {}, {}

        for pt, flowdir in pairs(self.simple_flood_expansion) do
            --if flowdir is nil for simple flood expansion, we have large problems.
            local newpt = pt + flowdir
            if IsValidSpotForFlood(newpt) then
                AddSimpleFlood(self.floodable_pt, new_simple_flood_expansion, radius, newpt, flowdir)
            else
                flood_flow_blocked[newpt] = -flowdir
            end
        end

        for pt, sourcedir in pairs(self.advanced_flood_expansion) do
            if sourcedir then
                local sideoffsets = _inverted_surrounding_vertical_offsets[sourcedir] == nil and _surrounding_vertical_offsets or _surrounding_horizontal_offsets
                local frontbackoffsets = sideoffsets ~= _surrounding_vertical_offsets and _surrounding_vertical_offsets or _surrounding_horizontal_offsets
                --spread outward
                local frontdir = -sourcedir
                local newpt = pt + frontdir
                if IsValidSpotForFlood(newpt) then
                    AddAdvancedFlood(self.floodable_pt, new_advanced_flood_expansion, radius, newpt, sourcedir, IsFloodAlone(self.floodable_pt, newpt, sideoffsets, frontdir))
                else
                    flood_flow_blocked[newpt] = sourcedir
                end
                --spread to the sides
                for i, offset in ipairs(sideoffsets) do
                    newpt = pt + offset
                    if IsValidSpotForFlood(newpt) then
                        local flowdir = sideoffsets == _surrounding_vertical_offsets and offset or frontdir
                        --simple floods only spread directly up and directly down
                        AddSimpleFlood(self.floodable_pt, new_simple_flood_expansion, radius, newpt, flowdir)
                        flood_test_alone[newpt] = -offset
                    else
                        flood_flow_blocked[newpt] = -offset
                    end
                end
            else
                --spread in every direction
                for i, offset in ipairs(_surrounding_offsets) do
                    local newpt = pt + offset
                    if IsValidSpotForFlood(newpt) then
                        AddAdvancedFlood(self.floodable_pt, new_advanced_flood_expansion, radius, newpt, -offset, true)
                    end
                end
            end
        end

        --we can't test for flood aloneness until all the floods have spawned, this will convert any simple floods into advanced floods that are alone.
        for pt, sourcedir in pairs(flood_test_alone) do
            local sideoffsets = _inverted_surrounding_vertical_offsets[sourcedir] == nil and _surrounding_vertical_offsets or _surrounding_horizontal_offsets
            if IsFloodAlone(self.floodable_pt, pt, sideoffsets, -sourcedir) then
                AddAdvancedFlood(self.floodable_pt, new_advanced_flood_expansion, radius, pt, sourcedir, true)
                new_simple_flood_expansion[pt] = nil
            end
        end

        for pt, sourcedir in pairs(flood_flow_blocked) do
            local origin = pt + sourcedir
            local sideoffsets = _inverted_surrounding_vertical_offsets[sourcedir] == nil and _surrounding_vertical_offsets or _surrounding_horizontal_offsets
            local frontbackoffsets = sideoffsets ~= _surrounding_vertical_offsets and _surrounding_vertical_offsets or _surrounding_horizontal_offsets
            for i, offset in ipairs(sideoffsets) do
                local flooddata = self.floodable_pt[origin + offset]
                if flooddata then
                    local newpt = origin + offset + ((-sourcedir) * (radius - flooddata.radius))
                    flooddata = self.floodable_pt[newpt]
                    if flooddata and new_simple_flood_expansion[newpt] then
                        new_simple_flood_expansion[newpt] = nil
                        AddAdvancedFlood(self.floodable_pt, new_advanced_flood_expansion, radius, newpt, -offset, IsFloodAlone(self.floodable_pt, newpt, frontbackoffsets, offset))
                    end
                end
            end
        end

        self.simple_flood_expansion = new_simple_flood_expansion
        self.advanced_flood_expansion = new_advanced_flood_expansion
    end
end

function Puddle:UpdateRadius()
    local radius = self:GetRadius()
    local targetradius = self:GetTargetRadius()

    if radius == targetradius then return end

    if radius < targetradius then --increasing
        if rawget(self.floodable_radius, radius + 1) == nil then return end --failsafe for if we somehow beat floodable radius in its size increase.
        radius = self:SetRadius(radius + 1)
        if self.flooded_radius[radius-1].size > 0 or radius-1 == 0 then
            for pt, _ in pairs(self.floodable_radius[radius]) do
                if self:CanQueueFloodSpawn(pt) then
                    self:QueueFloodSpawn(pt, radius)
                end
            end
        end
    else --decreasing
        self:SetRadius(radius - 1)
        for pt, _ in pairs(self.flooded_radius[radius]) do
            if self:CanQueueFloodDespawn(pt) then
                self:QueueFloodDespawn(pt, radius)
            end
        end
    end
end

function Puddle:UpdateSpawnFloods()
    --1% of the total floods to (de)spawn will range from 1(minimum) to 5.
    for i = 1, math.ceil((self.spawn_queue_count + self.despawn_queue_count) * 0.01) do --the more floods that are queued the faster they spawn.
        if self.despawn_queue_count == 0 or (self.spawn_queue_count > 0 and math.random() <= 0.5) then
            local pt, radius = next(self.spawn_queue)
            self:SpawnFlood(pt, radius)
            QueueFloodTileState(pt, true, self)
        else
            local pt, radius = next(self.despawn_queue)
            self:DespawnFlood(pt)
            QueueFloodTileState(pt, false, self)
        end
    end
end

function Puddle:OnUpdate(dt)
    if not _isfloodseason or _israining then
        if _isfloodseason then
            self.timeSinceIncrease = self.timeSinceIncrease + dt
            if self.timeSinceIncrease > _timeBetweenFloodIncreases and self:GetTargetRadius() < _targetPuddleHeight then
                self.timeSinceIncrease = GetRandomWithVariance(0, 3) --since lots of floods are spawning in the same tick, give a small variance to every floods update time.
                self:SetTargetRadius(self:GetTargetRadius() + 1)
            end
        else
            self.timeSinceDecrease = self.timeSinceDecrease + dt
            if self.timeSinceDecrease > _timeBetweenFloodDecreases and self:GetTargetRadius() > _targetPuddleHeight then
                self.timeSinceDecrease = GetRandomWithVariance(0, 3) --since all floods will swap to attempted decreasing in the same tick, give a small variance to every floods update time.
                self:SetTargetRadius(self:GetTargetRadius() - 1)
            end
        end
    end
    if not self.deadpuddle then
        self:DoFloodExpansion()
        self:UpdateRadius()

        self:UpdateSpawnFloods()
        if self.flooded_radius.size == 0 and self:GetRadius() == 0 and self:GetTargetRadius() == 0 then
            RemovePuddle(self)
        end
    end
end

function Puddle:SaveFloodable()
    local floodable = {}
    for k, v in pairs(self.floodable_pt) do
        floodable[#floodable + 1] = {x = k.x, y = k.y, r = v.radius, a = v.a, d = v.d, s = v.s}
    end
    return floodable
end

function Puddle:SaveFloods()
    local floods = {}
    for k, v in pairs(self.flooded_pt) do
        floods[#floods + 1] = {x = k.x, y = k.y, r = v}
    end
    return floods
end

function Puddle:Save()
    local data = {}
    data.pos = self.pos
    data.floodable = self:SaveFloodable()
    data.floods = self:SaveFloods()
    data.targetradius = self:GetTargetRadius()
    data.radius = self:GetRadius()
    data.deadpuddle = self.deadpuddle
    return data
end

function Puddle:InitializeFloodable(flooded)
    for i, pt in ipairs(flooded or {}) do
        local data = {radius = pt.r, a = pt.a, d = pt.d, s = pt.s}
        pt = GetFloodPoint(pt)
        self.floodable_pt[pt] = data
    end
    self.simple_flood_expansion = {}
    self.advanced_flood_expansion = {}
    for pt, _ in pairs(self.floodable_radius[#self.floodable_radius]) do
        local data = self.floodable_pt[pt]
        if data.a then
            self.advanced_flood_expansion[pt] = _surrounding_offsets[data.d] or false
        else
            self.simple_flood_expansion[pt] = _surrounding_offsets[data.d]
        end
    end
end

function Puddle:InitializeFloods(floods)
    for i, pt in ipairs(floods or {}) do
        local radius = pt.r
        pt = GetFloodPoint(pt)
        if IsValidSpotForFlood(pt) then
            self:SpawnFlood(pt, radius)
            QueueFloodTileState(pt, true, self)
        end
    end
end

function Puddle:Load(data)
    if data then
        self.pos = GetFloodPoint(data.pos)
        self:InitializeFloodable(data.floodable)
        self:InitializeFloods(data.floods)
        self:SetTargetRadius(data.targetradius)
        self:SetRadius(data.radius)
        self.timeSinceIncrease = data.timeSinceIncrease or GetRandomWithVariance(0, 3)
        self.timeSinceDecrease = data.timeSinceDecrease or GetRandomWithVariance(0, 3)
    end
end

--change this to define _targetTide instead
--[[
function self:GetTideHeight()

	local nightLength = GetClock():GetNightTime()
	local duskLength = GetClock():GetDuskTime()

	--Floods start at the beginning of evening and end at daybreak, in that time they interpolate to max height and back to zero
	local timepassed = 0

	local time = GetClock():GetNormTime()
	local tidePerc = 0
	local startGrowTime = 1.0 - 3/16
	local startShrinkTime = 0
	local endShrinkTime = 3/16

	if time > startGrowTime then
		tidePerc = (time - startGrowTime)/(3/16)
	elseif time > startShrinkTime and time < endShrinkTime then
		tidePerc = 1.0 - (time/(endShrinkTime - startShrinkTime))
	end

	return _maxTide * _maxTideMod * tidePerc
end
]]

local SpawnPuddleFromData = _ismastersim and function(data)
    data.pos = GetFloodPoint(data.pos)
    assert(data.pos)
	if _puddle_lookup[data.pos] then
		_puddle_lookup[data.pos]:SetTargetRadius(math.max(data.targetradius, _puddle_lookup[data.pos]:GetTargetRadius()))
		return false
	else
        local puddle = Puddle()
        puddle:Load(data)
		table.insert(_puddles, puddle)
		_puddle_lookup[data.pos] = puddle
		return true
	end
end

local TOTAL_TICKS_TO_SPAWN = 30
local SpawnRandomPuddles = _ismastersim and function()
    local endpoint = math.min(_worldfloodprogress + math.ceil(h * 2 / TOTAL_TICKS_TO_SPAWN), h * 2)
    for i = _worldfloodprogress, endpoint do
        for j = 0, w * 2 do
            if (math.random() * 4) < _spawnerFrequency then
                local pos = GetFloodPoint(j / 2 + .25, i / 2 + .25)
                if IsValidSpotForFlood(pos, true) then
                    SpawnPuddleFromData({pos = pos, targetradius = 2})
                end
            end
        end
        _worldfloodprogress = i
    end
    if endpoint == h * 2 then
        _worldfloodprogress = 0
        _floodedworld = true
    end
end

local function UpdateApplyFloodTileStates()
    for pt, val in pairs(_flood_queue) do
        ApplyFloodTileState(pt, val)
    end
end

--------------------------------------------------------------------------
--[[ Private event handlers ]]
--------------------------------------------------------------------------

--For when a sandbag or other floodblocker is removed
local floodblockerremoved =  _ismastersim and function(src, data)
    local pt = RealToFloodPos(data.blocker.Transform:GetWorldPosition())

	_blocker_lookup[pt] = nil
	--refresh any puddles touching the affected tile
	--flood doesn't (usually) survive if it gets blocked, so skip the tile itself
	local puddles = {}
	for i, offset in ipairs(_surrounding_offsets) do
        local newpt = pt + offset
        local tiledata = GetFloodData(newpt)
		if tiledata and tiledata.sources then
			for puddle, _ in pairs(tiledata.sources) do
                --just cause a flood tile is there doesn't mean that a flood could expand into that location (due to other blockers)
                if puddle.floodable_pt[newpt] then
                    if puddles[puddle] == nil then
                        puddles[puddle] = puddle.floodable_pt[newpt].radius
                    else
                        puddles[puddle] = math.min(puddle.floodable_pt[newpt].radius, puddles[puddle])
                    end
                end
			end
		end
	end

	for puddle, radius in pairs(puddles) do
        puddle:UpdateFloodable(radius+1)
	end
end

local floodblockercreated = _ismastersim and function(src, data)
	local pt = RealToFloodPos(data.blocker.Transform:GetWorldPosition())

	_blocker_lookup[pt] = true
	--check if flood gets removed
    local tiledata = GetFloodData(pt)
	if tiledata and tiledata.sources then  
		-- inform the puddles
		--[[  -- change it to let blocked flood no expansion  -Jerry
        for puddle, _ in pairs(tiledata.sources) do
            puddle:DespawnFlood(pt)
            if puddle.floodable_pt[pt] then
                puddle:UpdateFloodable(puddle.floodable_pt[pt].radius)
            end
		end
        --]]
        -- do all this at once
        ApplyFloodTileState(pt, false)
	end
end

local seasontick = _ismastersim and function(src, data)
    _seasonprogress = data.progress or 0
    _isfloodseason = data.season == "spring" and (_seasonprogress + _clockprogress) >= 0.25
        -- or data.season == "summer" and data.progress < 0.25 --summer is not necessarily the next season!
end

local clocktick = _ismastersim and function(src, data)
    _clockprogress = (data.time or 0) / TheWorld.state[TheWorld.state.season.."length"]
end

local moonphasechanged = _ismastersim and function(src, phase)
	assert(phase ~= nil)
	_maxTide = _moontideheights[phase] or 0
end

local precipitation_islandchanged = _ismastersim and function(src, bool)
	_israining = bool
end

--------------------------------------------------------------------------
--[[ Initialization ]]
--------------------------------------------------------------------------

--Register events
if _ismastersim then
	inst:ListenForEvent("floodblockerremoved", floodblockerremoved, _world)
	inst:ListenForEvent("floodblockercreated", floodblockercreated, _world)
    inst:ListenForEvent("seasontick", seasontick, _world)
    inst:ListenForEvent("clocktick", clocktick, _world)
	inst:ListenForEvent("moonphasechanged", moonphasechanged, _world)
	inst:ListenForEvent("precipitation_islandchanged", precipitation_islandchanged, _world)
end

inst:StartUpdatingComponent(self)

--------------------------------------------------------------------------
--[[ Public member functions ]]
--------------------------------------------------------------------------

if not _ismastersim then
    function self:AddFloodTile(tile)
        _flood_lookup[RealToFloodPos(tile.Transform:GetWorldPosition())] = tile
    end

    function self:RemoveFloodTile(tile)
        _flood_lookup[RealToFloodPos(tile.Transform:GetWorldPosition())] = nil
    end
end

function self:IsPointOnFlood(x, y, z)
    local floodpos = RealToFloodPos(x, y, z)
    return (_ismastersim and FloodTileExists(floodpos)) or _flood_lookup[floodpos] ~= nil
end

self.OnFlood = self.IsPointOnFlood

if _ismastersim then
    --mostly there in case something still tries to use it
    function self:GetIsFloodSeason()
        return _isfloodseason
    end

    function self:SetFloodSettings(maxLevel, frequency)
        _maxFloodLevel = math.min(maxLevel, TUNING.MAX_FLOOD_LEVEL)
        _spawnerFrequency = frequency
    end

    function self:SetMaxTideModifier(mod)
        _maxTideMod = mod
    end

    --must be entity-coords, not tile coords, targetradius is optional (2 is default)
    function self:SpawnPuddle(x, y, z, targetradius)
    	local pos = RealToFloodPos(x, y, z)
    	if IsValidSpotForFlood(pos, true) then
    		SpawnPuddleFromData({pos = pos, targetradius = targetradius or 2})
    	end
    end

    self.SetPositionPuddleSource = self.SpawnPuddle
end -- _ismastersim

function self:GetTileCenterPoint(x, y, z)
    return FloodToRealPos(RealToFloodPos(x, y, z)):Get()
end

function self:FloodToRealPos(x, y)
    return FloodToRealPos(GetFloodPoint(x, y)):Get()
end

function self:RealToFloodPos(x, y, z)
    local floodpos = RealToFloodPos(x, y, z)
    return floodpos.x, floodpos.y
end

--------------------------------------------------------------------------
--[[ Update ]]
--------------------------------------------------------------------------

function self:OnUpdate(dt)
	if _ismastersim then
	   --puddle growth is calculated serverside only
		if _isfloodseason then
			_targetPuddleHeight = math.ceil(_maxFloodLevel * math.max(0, ((_seasonprogress + _clockprogress) - 0.25) / 0.75)) --Don't spawn floods in the first 1/4 of the season
            if _forcedFloodLevel then
                _targetPuddleHeight = _forcedFloodLevel
            end
            if _worldfloodprogress > 0 or (_israining and not _floodedworld and _worldfloodprogress == 0 and _targetPuddleHeight >= 2) then
                SpawnRandomPuddles()
            end
		else
			_targetPuddleHeight = 0
            _floodedworld = false
		end

		for i, puddle in ipairs(_puddles) do
            puddle:OnUpdate(dt)
		end
    end

    UpdateApplyFloodTileStates()

	--Tides stuff
	-- local currentHeight = GetWorld().Flooding:GetTargetDepth()
	-- local newHeight = self:GetTideHeight()
	-- GetWorld().Flooding:SetTargetDepth(newHeight)

	-- if newHeight < currentHeight then
		-- --Flood receding
	-- end

	-- if newHeight == 0 and GetIsFloodSeason() then
		-- self:SwitchMode("flood")
	-- end

	-- self.inst:PushEvent("floodChange")
end

self.LongUpdate = self.OnUpdate

--------------------------------------------------------------------------
--[[ Save/Load ]]
--------------------------------------------------------------------------

if _ismastersim then

    function self:OnSave()
    	local data = {}

    	data._targetPuddleHeight = _targetPuddleHeight
        data._worldfloodprogress = _worldfloodprogress
        data._floodedworld = _floodedworld
    	data._puddles = {}
        for i, puddle in ipairs(_puddles) do
            data._puddles[i] = puddle:Save()
        end

    	return data
    end

    function self:OnLoad(data)
    	if data ~= nil then
            --migrate old savedata.
            if data._worldfloodprogress == nil and data._floodedworld == nil then
                data._targetPuddleHeight = data._targetPuddleHeight or 0
                data._worldfloodprogress = 0
                data._floodedworld = #data._puddles > 0 and data._targetPuddleHeight > 0 --assume world was already flooded this season if a single puddle exists... :( -Z
                data._puddles = {}
                --trying to port over the old flood save data would not even be worth the effort, they get a "free" flood season in the event that this happens
            end
            _targetPuddleHeight = data._targetPuddleHeight
            _worldfloodprogress = data._worldfloodprogress
            _floodedworld = data._floodedworld
			for i, puddle in pairs(data._puddles or {}) do
				SpawnPuddleFromData(puddle) --calls puddle:Load(data)
			end
    	end
    end

end -- _ismastersim

--------------------------------------------------------------------------
--[[ Debug ]]
--------------------------------------------------------------------------

function self:GetDebugString()
	return string.format("tides: %d/%d, max %d, mod %d", _currentTide, _targetTide, _maxTide, _maxTideMod)
end

function self:ShowPuddles()
	--shows all puddles on the map, as rawling
	for i, puddle in ipairs(_puddles) do
        local source = puddle:GetSourceFlood()
		if source then
			local minimap = GetNetworkTileState(source.x, source.y).inst.entity:AddMiniMapEntity()
			minimap:SetIcon("rawling.tex")
		end
	end
end

function self:P(player)
    print(RealToFloodPos(player.Transform:GetWorldPosition()))
end

function self:RecalculateFloodable()
    for i, puddle in ipairs(_puddles) do
        puddle:UpdateFloodable(1)
    end
end

function self:ForcePuddleHeight(level)
    _forcedFloodLevel = level
end

function self:RemoveAllPuddles()
    for i, puddle in ipairs(_puddles) do
        puddle:SetTargetRadius(0)
        for pt, _ in pairs(puddle.flooded_pt) do
            local tiledata = GetFloodData(pt)
            if tiledata and tiledata.sources then
                ApplyFloodTileState(pt, false)
                _puddles[i] = nil
            end
        end
    end
end

--------------------------------------------------------------------------

end)