local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Placer = require("components/placer")

local _GetDeployAction = Placer.GetDeployAction
function Placer:GetDeployAction(...)
    local action = _GetDeployAction(self, ...)
    if self.invobject.replica.inventoryitem then
        local deploydistance = self.invobject.replica.inventoryitem:GetDeployDist()
        if deploydistance ~= 0 then
            action.distance = deploydistance
        end
    end
    return action --zark made an "opsie" and forgot to return the action -Half
end
