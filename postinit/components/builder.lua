local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Builder = require("components/builder")

--Zarklord: blindly replace this function, since idk what else to do...
function Builder:MakeRecipeAtPoint(recipe, pt, rot, skin)
    if recipe.placer ~= nil and
        self:KnowsRecipe(recipe.name) and
        self:IsBuildBuffered(recipe.name) and
        TheWorld.Map:CanDeployRecipeAtPoint(pt, recipe, rot, self.inst) then
        self:MakeRecipe(recipe, pt, rot, skin)
    end
end

local _GetIngredientWetness = Builder.GetIngredientWetness
function Builder:GetIngredientWetness(ingredients, ...)
    if IsInIAClimate(self.inst) then
        local _wetness = rawget(TheWorld.state, "wetness")
        TheWorld.state.wetness = TheWorld.state.islandwetness
        local rets = {_GetIngredientWetness(self, ingredients, ...)}
        TheWorld.state.wetness = _wetness
        return unpack(rets)
    end
    return _GetIngredientWetness(self, ingredients, ...)
end

local _KnowsRecipe = Builder.KnowsRecipe
function Builder:KnowsRecipe(recipe, ...)
    local knows = _KnowsRecipe(self, recipe, ...)
    if not knows and self.jellybrainhat then
        if type(recipe) == "string" then
            recipe = GetValidRecipe(recipe)
        end

        if recipe == nil then
            return false
        end

        local valid_tech = true
        for techname, level in pairs(recipe.level) do
            if level ~= 0 and (TECH.LOST[techname] or 0) == 0 then
                valid_tech = false
                break
            end
        end
        for i, v in ipairs(recipe.tech_ingredients) do
            if not self:HasTechIngredient(v) then
                valid_tech = false
                break
            end
        end
        if string.find(recipe.name, "hermitshop") then
            valid_tech = false
        end
        knows = valid_tech and (recipe.builder_tag == nil or self.inst:HasTag(recipe.builder_tag))
    end
    return knows
end

local _BufferBuild = Builder.BufferBuild
function Builder:BufferBuild(recname, ...)
    local recipe = GetValidRecipe(recname)
   local shouldevent = recipe ~= nil and recipe.placer ~= nil and not self:IsBuildBuffered(recname) and self:HasIngredients(recipe)-- and self:CanBuild(recname)
    _BufferBuild(self, recname, ...)
    if shouldevent then
        self.inst:PushEvent("bufferbuild", {recipe = recipe})
    end
end

local _isloading = setmetatable({}, {__mode = "k"}) --use a weak table so that we can't ever force these objects to stay in existance.

local _AddRecipe = Builder.AddRecipe
function Builder:AddRecipe(recname, ...)
    if not self.jellybrainhat or _isloading[self] then
        return _AddRecipe(self, recname, ...)
    end
end

local _OnLoad = Builder.OnLoad
function Builder:OnLoad(data, ...)
    _isloading[self] = true
    _OnLoad(self, data, ...)
    _isloading[self] = nil
end

if IA_CONFIG and not IA_CONFIG.fixnoplayerboats then --Hornet: no need for this code if the sea yard works on boats without players on them
    local _OnUpdate = Builder.OnUpdate
    function Builder:OnUpdate(dt, ...)
        _OnUpdate(self, dt, ...)
        self:EvaluateAutoFixers()
    end

    function Builder:EvaluateAutoFixers()
        local pos = self.inst:GetPosition()
        local ents = TheSim:FindEntities(pos.x, pos.y, pos.z, TUNING.SEA_YARD_REPAIR_DISTANCE, { "autofixer" }, self.exclude_tags)

        local old_fixer = self.current_fixer
        self.current_fixer = nil

        local fixer_active = false
        for k, v in pairs(ents) do
            if v.components.autofixer then
                if not fixer_active and v.components.autofixer:CanAutoFixUser(self.inst) then
                    --activate the first machine in the list. This will be the one you're closest to.
                    v.components.autofixer:TurnOn(self.inst)
                    fixer_active = true
                    self.current_fixer = v

                else
                    --you've already activated a machine. Turn all the other machines off.
                    v.components.autofixer:TurnOff(self.inst)
                end
            end
        end

        if old_fixer ~= nil and
            old_fixer ~= self.current_fixer and
            old_fixer.components.autofixer ~= nil and
            old_fixer.entity:IsValid() then
            old_fixer.components.autofixer:TurnOff(self.inst)
        end
    end
end


local function onjellybrainhat(self, jellybrainhat)
    self.inst.replica.builder:SetIsJellyBrainHat(jellybrainhat)
end

IAENV.AddComponentPostInit("builder", function(cmp)

    addsetter(cmp, "jellybrainhat", onjellybrainhat)
    cmp.jellybrainhat = false

    --ignore flooded prototypers
    table.insert(cmp.exclude_tags, "flooded")
end)
