local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local oldCopy
local function Copy(self, rhs)
	oldCopy(self, rhs)

	rhs = rhs ~= nil and rhs.components.recallmark
	if rhs then
		self.recall_onwater = rhs.recall_onwater
	end
end

local oldMarkPosition
local function MarkPosition(self, recall_x, recall_y, recall_z, recall_worldid)
	if recall_x ~= nil and recall_y ~= nil and recall_z ~= nil then
		self.recall_onwater = (IsOnWater(recall_x, recall_y, recall_z) == true and true) or false 
	end
	oldMarkPosition(self, recall_x, recall_y, recall_z, recall_worldid)
end

local oldOnSave
local function OnSave(self)
	local data = oldOnSave(self)
	data.recall_onwater = self.recall_onwater
	return data
end

local oldOnLoad
local function OnLoad(self, data)
	oldOnLoad(self, data)
	self.recall_onwater = data.recall_onwater
end

local function fn(self)
	oldCopy = self.Copy
	self.Copy = Copy

	oldMarkPosition = self.MarkPosition
	self.MarkPosition = MarkPosition

	oldOnSave = self.OnSave
	self.OnSave = OnSave

	oldOnLoad = self.OnLoad
	self.OnLoad = OnLoad
end

IAENV.AddComponentPostInit("recallmark", fn)
