local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local AmphibiousCreature = require("components/amphibiouscreature")

local _OnUpdate = AmphibiousCreature.OnUpdate
function AmphibiousCreature:OnUpdate(dt)
	--copy of original code, except it tests for SW water
	if self.inst.sg == nil or not self.inst.sg:HasStateTag("jumping") then
		local x, y, z = self.inst.Transform:GetWorldPosition()
		local is_on_water = IsOnWater(x, y, z)

		if is_on_water then
			if not self.in_water then
				self:OnEnterOcean()
			end
			return --must not run original code, lest it mistakes SW water for land
		-- elseif self.in_water then --handled by original code
			-- self:OnExitOcean()
		end
	end
	
    return _OnUpdate(self, dt)
end

IAENV.AddComponentPostInit("amphibiouscreature", function(cmp)
    cmp.inst:AddTag("amphibious")
end)
