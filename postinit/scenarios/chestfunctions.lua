local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local chestfunctions = require("scenarios/chestfunctions")

local _AddChestItems = chestfunctions.AddChestItems
function chestfunctions.AddChestItems(chest, loot, num, ...)
	for k, itemtype in ipairs(loot) do
		local old_initfn = itemtype.initfn
		itemtype.initfn = function(item, ...)
			if chest and item.components.visualvariant then
				item.components.visualvariant:CopyOf(chest, true)
			end
			return old_initfn and old_initfn(item, ...)
		end
	end
    return _AddChestItems(chest, loot, num, ...)
end
