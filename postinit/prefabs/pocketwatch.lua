local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------
--TUNING.POCKETWATCH_RECALL_COOLDOWN = 0

local function GetClosestBoatInRange(x, y, z, range)
	local ents = TheSim:FindEntities(x, y, z, range, {"sailable"})
	return ents[1]
end

local function ShouldWarp(inst, doer, onwater) -- Check if the area is safe.
	local recallmark = inst.components.recallmark
	local PlayerOnWater = doer.components.sailor.sailing
	local TargetOnWater = onwater
	local x, y, z = nil

	if recallmark ~= nil then
		x, y, z = recallmark.recall_x, recallmark.recall_y, recallmark.recall_z
	else
		x, y, z = doer.components.positionalwarp:GetHistoryPosition(false)
	end

	if PlayerOnWater == TargetOnWater then
		return true, "Normal"
	elseif PlayerOnWater == true and TargetOnWater == false then
		return true, "ToLand"
	elseif PlayerOnWater == false and TargetOnWater == true then
		if recallmark ~= nil and (not recallmark:IsMarkedForSameShard()) then
			return true, "OtherShardToWater" -- Niko: Temporary fix so I don't have to ask the other shard if there are boats near there.
		else
			if GetClosestBoatInRange(x, y, z, TUNING.POCKETWATCH_BOATRANGE) ~= nil then -- Niko: Eventually maybe replace this with a indevidual value for each watch.
				return true, "ToWater"
			else
				return false, "WATER_UNSAFE"
			end
		end
	end
end

local function Warp_DoCastSpell(inst, doer, target, pos)
	local tx, ty, tz = doer.components.positionalwarp:GetHistoryPosition(false)
	if tx ~= nil then
		local TargetOnWater = (IsOnWater(tx, ty, tz) == true and true) or false
		

		local suc, act = ShouldWarp(inst, doer, TargetOnWater)
		if suc then
			if act == "ToLand" then -- If the player is going from water to land...
				doer.components.sailor:Disembark() -- Leave the boat behind...
				return inst.ActualSpell(inst, doer) -- Then do the usual stuff.

			elseif act == "ToWater" then -- If the player is going from land to water

				-- Niko: We may want to replace the static time with something a bit more dynamic.
				inst:DoTaskInTime(20 * FRAMES, function() -- Once we have teleported...
					local x, y, z = doer.Transform:GetWorldPosition()
					local boat = GetClosestBoatInRange(x, y, z, TUNING.POCKETWATCH_BOATRANGE) or SpawnPrefab("boat_lograft")
					if boat ~= nil then
						doer.components.sailor:Embark(boat) -- Mount the player on a nearby boat, if there is none, spawn a basic one and mount that...
					end
				end)

				return inst.ActualSpell(inst, doer) -- But first do the usual stuff.

			elseif act == "OtherShardToWater" then
				doer.WarpOtherShardToWater = true
				return inst.ActualSpell(inst, doer)
			end
			return inst.ActualSpell(inst, doer) -- If the player is going from land to land or water to water, don't worry about a thing and do all as usual.
		else
			return suc, act -- The target water was deemed unsafe, prevent the player from teleporting.
		end
	else
		return inst.ActualSpell(inst, doer) -- This should fail due to there being no warp points left, so I'll just let the usual stuff run in case a mod uses that.
	end
end

local function Recall_DoCastSpell(inst, doer, target, pos)
	local recallmark = inst.components.recallmark

	if recallmark:IsMarked() then
		local suc, act = ShouldWarp(inst, doer, recallmark.recall_onwater)
		
		if suc then
			if act == "ToLand" then -- If the player is going from water to land...
				doer.components.sailor:Disembark() -- Leave the boat behind...
				return inst.ActualSpell(inst, doer) -- Then do the usual stuff.

			elseif act == "ToWater" then -- If the player is going from land to water
				-- Niko: We may want to replace the static time with something a bit more dynamic.
				inst:DoTaskInTime(20 * FRAMES, function() -- Once we have teleported...
					local x, y, z = doer.Transform:GetWorldPosition()
					local boat = GetClosestBoatInRange(x, y, z, TUNING.POCKETWATCH_BOATRANGE) or SpawnPrefab("boat_lograft")
					if boat ~= nil then
						doer.components.sailor:Embark(boat) -- Mount the player on a nearby boat, if there is none, spawn a basic one and mount that...
					end
				end)

				return inst.ActualSpell(inst, doer) -- But first do the usual stuff.

			elseif act == "OtherShardToWater" then
				doer.WarpOtherShardToWater = true
				return inst.ActualSpell(inst, doer)
			end
			return inst.ActualSpell(inst, doer) -- If the player is going from land to land or water to water, don't worry about a thing and do all as usual.
		else
			return suc, act -- The target water was deemed unsafe, prevent the player from teleporting.
		end
	else
		return inst.ActualSpell(inst, doer) -- If there was no mark set, leave it do it's usual thing.
	end
end

---- Needed for the portal spell.
local NOTENTCHECK_CANT_TAGS = { "FX", "INLIMBO" }
local function noentcheckfn(pt)
    return not TheWorld.Map:IsPointNearHole(pt) and #TheSim:FindEntities(pt.x, pt.y, pt.z, 1, nil, NOTENTCHECK_CANT_TAGS) == 0
end
local function DelayedMarkTalker(player)
	-- if the player starts moving right away then we can skip this
	if player.sg == nil or player.sg:HasStateTag("idle") then 
		player.components.talker:Say(GetString(player, "ANNOUNCE_POCKETWATCH_MARK"))
	end 
end
----

local function portal_DoCastSpell(inst, doer, target, pos)
	-- Niko: For now I'll just manually overwrite the entire spell.
	local recallmark = inst.components.recallmark

	if recallmark:IsMarked() then
		local pt = inst:GetPosition()
		local offset = FindWalkableOffset(pt, math.random() * 2 * PI, 3 + math.random(), 16, false, true, noentcheckfn, true, true)
						or FindWalkableOffset(pt, math.random() * 2 * PI, 5 + math.random(), 16, false, true, noentcheckfn, true, true)
						or FindWalkableOffset(pt, math.random() * 2 * PI, 7 + math.random(), 16, false, true, noentcheckfn, true, true)
		if offset ~= nil then
			pt = pt + offset
		end

		if not Shard_IsWorldAvailable(recallmark.recall_worldid) then
			return false, "SHARD_UNAVAILABLE"
		end

		local portal = SpawnPrefab("pocketwatch_portal_entrance")
		portal.Transform:SetPosition(pt:Get())
		portal:SpawnExit(recallmark.recall_worldid, recallmark.recall_x, recallmark.recall_y, recallmark.recall_z, recallmark.recall_onwater)
		inst.SoundEmitter:PlaySound("wanda1/wanda/portal_entrance_pre")

        local new_watch = SpawnPrefab("pocketwatch_recall")
		new_watch.components.recallmark:Copy(inst)

		local x, y, z = inst.Transform:GetWorldPosition()
        new_watch.Transform:SetPosition(x, y, z)
		new_watch.components.rechargeable:Discharge(TUNING.POCKETWATCH_RECALL_COOLDOWN)

        local owner = inst.components.inventoryitem ~= nil and inst.components.inventoryitem.owner or nil
        local holder = owner ~= nil and (owner.components.inventory or owner.components.container) or nil
        if holder ~= nil then
            local slot = holder:GetItemSlot(inst)
            inst:Remove()
            holder:GiveItem(new_watch, slot, Vector3(x, y, z))
        else
            inst:Remove()
        end

		return true
	else
		local x, y, z = doer.Transform:GetWorldPosition()
		recallmark:MarkPosition(x, y, z)
		inst.SoundEmitter:PlaySound("wanda2/characters/wanda/watch/MarkPosition")

		doer:DoTaskInTime(12 * FRAMES, DelayedMarkTalker) 

		return true
	end
end

local function portal_onActivate(inst, doer)
	inst.oldonActivte(inst, doer)
	local PlayerOnWater = doer.components.sailor.sailing
	local TargetOnWater = inst.TargetOnWater
	if inst.components.teleporter.migration_data ~= nil and TargetOnWater == true then -- To other shard and to water
		doer.WarpOtherShardToWater = true
	end
	if not PlayerOnWater == TargetOnWater then
		if TargetOnWater == false then -- Water to land
			doer.components.sailor:Disembark() -- Niko: This runs after the jump animation, it does not look great, but it at least works.

		elseif TargetOnWater == true then -- Land to water
			doer:DoTaskInTime(4.5, function() -- Once we have teleported...
				local x, y, z = doer.Transform:GetWorldPosition()
				local boat = GetClosestBoatInRange(x, y, z, TUNING.POCKETWATCH_BOATRANGE) or SpawnPrefab("boat_lograft")
				if boat ~= nil then
					doer.components.sailor:Embark(boat) -- Mount the player on a nearby boat, if there is none, spawn a basic one and mount that...
				end
			end)

		end
	end
end

local function portal_SpawnExit(inst, worldid, x, y, z, onwater)
	inst.oldSpawnExit(inst, worldid, x, y, z)
	inst.TargetOnWater = onwater
end

local function warp_fn(inst)
	if not TheWorld.ismastersim then return end -- Do not run on client

	inst.ActualSpell = inst.components.pocketwatch.DoCastSpell -- Backup the old function
	inst.components.pocketwatch.DoCastSpell = Warp_DoCastSpell -- Run our override
end

local function recall_fn(inst)
	if not TheWorld.ismastersim then return end -- Do not run on client

	inst.ActualSpell = inst.components.pocketwatch.DoCastSpell -- Backup the old function
	inst.components.pocketwatch.DoCastSpell = Recall_DoCastSpell -- Run our override

end

local function portal_fn(inst)
	if not TheWorld.ismastersim then return end -- Do not run on client

	-- inst.ActualSpell = inst.components.pocketwatch.DoCastSpell -- We don't need to back up the function sense we are completely replacing it.
	inst.components.pocketwatch.DoCastSpell = portal_DoCastSpell -- Run our override
end

local function portal_entrance_fn(inst)
	if inst.components.teleporter then
		inst.oldonActivte = inst.components.teleporter.onActivate
		inst.components.teleporter.onActivate = portal_onActivate
	end
	inst.oldSpawnExit = inst.SpawnExit
	inst.SpawnExit = portal_SpawnExit
	inst:AddTag("portal_entrance") -- Need this to find it with TheSim:FindEntities
end


IAENV.AddPrefabPostInit("pocketwatch_warp", warp_fn) -- Backstep
IAENV.AddPrefabPostInit("pocketwatch_recall", recall_fn) -- Backtreck
IAENV.AddPrefabPostInit("pocketwatch_portal", portal_fn) -- Rift
IAENV.AddPrefabPostInit("pocketwatch_portal_entrance", portal_entrance_fn) -- Rift portal entrance
