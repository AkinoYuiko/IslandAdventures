local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function fn(inst)
    --inst:AddTag("amphibious") --makes bernie walk on water TODO: add shadow platform/state
end
IAENV.AddPrefabPostInit("bernie_active", fn)
IAENV.AddPrefabPostInit("bernie_big", fn)
