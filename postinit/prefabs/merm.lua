local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local Old_OnSave
local function OnSave(inst,data)
    if inst:HasTag("save_loot") and inst.components.lootdropper and inst.components.lootdropper.loot then
        data.loot = inst.components.lootdropper.loot
    end
    if Old_OnSave then
        Old_OnSave(inst, data)
    end
end

local Old_OnLoad
local function OnLoad(inst,data)
    if data and data.loot then
        inst.components.lootdropper:SetLoot(data.loot)
        inst:AddTag("save_loot")
    end
    if Old_OnLoad then
        Old_OnLoad(inst,data)
    end
end


local function merm_postinit(inst)

    inst:AddTag("mermfighter")

    if TheWorld.ismastersim then
        --atm merms dont save or load anything -half
        if inst.OnSave then Old_OnSave = inst.OnSave end
        if inst.OnLoad then Old_OnLoad = inst.OnLoad end
        inst.OnSave = OnSave
        inst.OnLoad = OnLoad
    end

end

IAENV.AddPrefabPostInit("merm", merm_postinit)
IAENV.AddPrefabPostInit("mermguard", merm_postinit)
