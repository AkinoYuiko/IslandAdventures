local L = locale
local function translate(language_table)  -- use this fn can be automatically translated according to the language in the table
	language_table.zhr = language_table.zh
	language_table.zht = language_table.zht or language_table.zh
	return language_table[L] or language_table.en
end
-- {en = "", zh = "", zht = "", fr = "", es = "", de = "", it = "" , pt = "", pl = "", ru = "", ko = ""}

-- Mod Name
name = translate({en = "Island Adventures", zh = "岛屿冒险"})

-- Mod Authors
author = "Atlantic Aristocracy"

-- Mod Version
version = "0.8.8"
version_title = translate({
	en = "We're Gonna Need A Bigger Raft (Open Beta)",
	zh = "我们需要一个更大的木筏！(公测版)",
	pl = "Będziemy Potrzebowali Większej Tratwy (Otwarta Beta)"  -- just a try
})

-- Mod Description
description = translate({
	en = "Embark on a tropical journey across the seas, Together! Island Adventures brings the seas of Don't Starve: Shipwrecked to you!\n\nShould you encounter a problem, please tell us everything about the problem so we can repair things!",
	zh = "一起踏上穿越海洋的热带之旅！岛屿冒险为你带来饥荒：海难的海洋!\n\n 如果你遇到问题,请告诉我们有关问题的一切，这样我们就可以修复它\n\n"
})

description = description .. "\n\nVersion: " .. version .. "\n\"" .. version_title .. "\""

-- In-game link to a thread or file download on the Klei Entertainment Forums
forumthread = "/topic/95080-island-adventures-the-shipwrecked-port/"

IslandAdventures = true

folder_name = folder_name or "workshop-"
if not folder_name:find("workshop-") then
	--name = "1 " .. name .. " - GitLab Ver." -- DST mod menu now natively supports pinning mods to top.
	name = name .. " - GitLab Ver." -- DST mod menu now natively supports pinning mods to top.
	description = description .. "\n\nRemember to manually update! The version number does NOT increase with every gitlab update."
	IslandAdventuresGitlab = true
end

-- Don't Starve API version
-- Note: We set this to 10 so that it's incompatible with single player.
api_version = 10
-- Don't Starve Together API version
api_version_dst = 10

-- Priority of which our mod will be loaded
-- Below 0 means other mods will override our mod by default.
-- Above 0 means our mod will override other mods by default.
priority = 2

-- Forces user to reboot game upon enabling the mod
restart_required = false

-- Engine/DLC Compatibility
-- Don't Starve (Vanilla, no DLCs)
dont_starve_compatible = false
-- Don't Starve: Reign of Giants
reign_of_giants_compatible = false
-- Don't Starve: Shipwrecked
shipwrecked_compatible = false
-- Don't Starve Together
dst_compatible = true

-- Client-only mods don't affect other players or the server.
client_only_mod = false
-- Mods which add new objects are required by all clients.
all_clients_require_mod = true

-- Server search tags for the mod.
server_filter_tags =
{
    "island_adventures",
	"island adventures",
	"island",
	"adventures",
	"shipwrecked",
}

-- Preview image
icon_atlas = "ia-icon.xml"
icon = "ia-icon.tex"

mod_dependencies = {
    {--GEMCORE
        workshop = "workshop-1378549454",
        ["GemCore"] = false,
        ["[API] Gem Core - GitLab Version"] = true,
    },
}


-- Thanks to the Gorge Extender by CunningFox for making me aware of this being possible -M
local emptyoptions = {{description = "", data = false}}
local function Breaker(title, hover)
	return {
		name = title,
		hover = hover, --hover does not work, as this item cannot be hovered
		options = emptyoptions,
		default = false,
	}
end

local options_enable = {
	{description = translate({en = "Disabled", zh = "关闭"}), data = false},
	{description = translate({en = "Enabled", zh = "开启"}), data = true},
}

configuration_options =
{
	Breaker(translate({en = "Tweaks & Changes", zh = "调整和改变"})),
	{
        name = "openvolcano",
        label = translate({
			en = "Volcano",
			zh = "开启火山"
		}),
        hover = translate({
			en = "Caves needs to be enabled. If you change this option, you need to reenable this mod.",
			zh = "需要开启洞穴，更改此选项或移除洞穴,要重新勾选MOD"
		}),
        options = options_enable,
        default = true,
    },
	{
		name = "newplayerboats",
		label = translate({
			en = "Rafts for New Players",
			zh = "出生自带木筏"
		}),
        hover = translate({
			en = "Newly spawned players get given a pre-crafted Log Raft to leave their starting island on.",
			zh = "新玩家会得到一个木筏，离开出发岛"
		}),
        options = options_enable,
		default = true,
	},
	{
		name = "oldwarly",
		label = translate({
			en = "Pre-Official Warly",
			zh = "旧版沃利"
		}),
        hover = translate({
			en = "This mod had Warly before he was announced as an official DST character. Use this option to restore the IA Warly.",
			zh = "使用IA旧版沃利"
		}),
        options = options_enable,
		default = false,
	},
	{
		name = "tuningmodifiers",
		label = translate({
			en = "Combat Modifiers",
			zh = "战斗调整"
		}),
		hover = translate({
			en = "Monsters have more health, bosses deal less damage, and armour breaks faster. Klei decided that, we're just playing along.",
			zh = "怪物的生命值更高,BOSS造成的伤害更少,盔甲被破坏的速度更快"
		}),
		options = options_enable,
		default = true,
	},
	{
		name = "tigerbalance",
		label = translate({
			en = "Tigershark Buff",
			zh = "虎鲨加强"
		}),
        hover =  translate({
			en = "Tigershark's laughable attack range gets increased, making it a dangerous foe.",
			zh = "增加虎鲨攻击范围"
		}),
        options =
        {
            {description = translate({en = "Vanilla", zh = "普通"}), data = "vanilla"},
			{description = translate({en = "Medium",  zh = "中等"}), data = "medium"},
            {description = translate({en = "Hard", zh = "困难"}), data = "hard"},
        },
        default = "medium",
	},
	{
		name = "krakenbuff",
		label = translate({
			en = "Quacken Buff",
			zh = "海妖加强"
		}),
        hover = translate({
			en = "Quacken attacks faster, his tentacles range, attack speed and damage gets increased, making him more dangerous.",
			zh = "增强海妖的攻速，海妖触手的攻速，攻击力、血量、攻速、攻击范围"
		}),
        options = options_enable,
		default = true,
	},
	{
		name = "fixnoplayerboats",
		label = translate({
			en = "Sea Yard Buff",
			zh = "修船厂更改",
		}),
        hover = translate({
			en = "Sea Yards will continue to repair boats without players on them.",
			zh = "修船厂不需要玩家在船上也可以修船"
		}),
        options = options_enable,
		default = true,
	},
	{
		name = "windstaffbuff",
		label = translate({
			en = "Sail Stick SpeedBonus",
			zh = "桅杆加速效果"
		}),
        hover = translate({
			en = "The sail stick is now a powerfull tool that will help you and you're freinds sail the seas.",
			zh = "选择桅杆的加速倍数"
		}),
        options =
        {
			{description = "None", data = 1},
			{description = "1.5x", data = 1.5},
			{description = "2x", data = 2},
            {description = "3x", data = 3},
        },
		default = 2,
	},
	{
        name = "krakenhealth",
        label = translate({
			en = "Quacken Health",
			zh = "海妖血量"
		}),
        hover = translate({
			en = "Choose how much health Quacken has.",
			zh = "设置海妖血量"
		}),
        options =
        {
            {description = "1000", data = 1000},
			{description = "2000", data = 2000},
            {description = "4000", data = 4000},
            {description = "8000", data = 8000},
            {description = "12000", data = 12000},
        },
        default = 4000,
    },
    {
        name = "droplootground", --could be extended to other loot too
        label = translate({
			en = "Drop Hacked Bamboo & Vines",
			zh = "掉落竹子和藤蔓"
		}),
        hover   = translate({
			en = "Hacking bamboo or vines does not give the loot directly to the hacker, instead drops it on the floor.",
			zh = "砍伐竹子和藤蔓掉落在地上，而不是直接进入玩家背包"
		}),
        options = options_enable,
        default = true,
    },
	{
		name = "limestonerepair",
		label = translate({
			en = "Limestone Repairs",
			zh = "石灰石墙的修补"}),
        hover = translate({
			en = "Coral and Limestone can be used to repair Limestone Walls and Sea Walls.",
			zh = "珊瑚和石灰石可用于修复石灰岩墙和海上墙"
		}),
        options = options_enable,
		default = true,
	},
	{
		name = "windgustable",
		label = translate({
			en = "Wind Mechanics",
			zh = "风力学"
		}),
        hover = translate({
			en = "Choose what can be affected by strong winds, mainly for builders and people who are experiencing lag.",
			zh = "选择受强风影响的物品，主要针对建筑和刮风很卡的人。"
		}),
        options =
        {
			{description = translate({en = "All", zh = "所有"}),
				hover = translate({en = "Everything enabled.", zh = "所有东西都会受到风的影响"}) ,
				data = "all"
			},
			{
				description = translate({en = "NoWalls", zh = "排除墙"}),
				hover = translate({en = "NoWalls", zh = "墙不会被强风破坏"}),
				data = "nowalls"
			},
			{
				description = translate({en = "NoItems", zh = "排除物品"}),
				hover = translate({en = "Walls won't be damaged by strong winds.", zh = "物品不会被强风吹走，可能有助于缓解卡顿"}),
				data = "noitems"
			},
			{
				description = translate({en = "NoWalls-NoItems", zh = "排除墙和物品"}),
				hover = translate({en = "Walls won't be damaged and items won't be blown.", zh = "物品不会被强风吹走，墙不会被强风破坏"}),
				data = "nowallsnoitems"
			},
            {	description = translate({en = "None", zh = "关闭所有"}),
				hover = translate({en = "Nothing will be picked, hacked, zhoped, hammered or blown by strong winds.", zh = "风不会影响任何事物,包括人物的移速"}),
				data = "none"
			},
        },
		default = "all",
	},
	{
		name = "aquaticplacedstwater",
		label = translate({
			en = "SW boats on DST water",
			zh = "海难船和海上建筑的放置"
		}),
        hover = translate({
			en = "Allow SW boats and structures to be placed on DST water",
			zh ="海难的船和海上建筑可放在联机的海"
		}),
        options = options_enable,
		default = false
	},
	{
		name = "pondfishable",
		label = translate({
			en = "RoT-Style Fish",
			zh = "RoT-Style Fish"
		}),
        hover = translate({
			en = "Enable/Disable RoT-Style Tropical Fish",
			zh = "Enable/Disable RoT-Style Tropical Fish",
		}),
        options = options_enable,
		default = true
	},

	Breaker(translate({en = "Misc.", zh = "杂项"})),
    {
        name = "locale",
        label = translate({
			en = "Force Translation",
			zh = "强制翻译"
		}),
        hover = translate({
			en = "Select a translation to enable it regardless of language packs.",
			zh = "选择翻译以启用它，而不考虑语言"
		}),
        options =
		{
			{description = "None", data = false},
			{description = "Deutsch", data = "de"},
			{description = "Español", data = "es"},
			{description = "Français", data = "fr"},
			{description = "Italiano", data = "it"},
			{description = "한국어", data = "ko"},
			{description = "Polski", data = "pl"},
			{description = "Português", data = "pt"},
			{description = "Русский", data = "ru"},
			{description = "中文 (简体)", data = "sc"},
			{description = "中文 (繁体)", data = "tc"},
		},
        default = false,
    },
    {
        name = "dynamicmusic",
        label = translate({
			en = "Dynamic Music",
			zh = "音乐"
		}),
        hover   = translate({
			en = "If you have problems using IA with other Music Mods, disable this. The unique Combat and Work music will not play.",
			zh = "若与其他音乐MOD一起使用时遇到问题,请禁用此功能。海难音乐不会播放"
		}),
        options = options_enable,
        default = true,
    },
	{
		name = "devmode",
		label = translate({
			en = "Dev Mode",
			zh = "开发模式"
		}),
        hover = translate({
			en = "Enable this to turn your keyboard into a minefield of crazy debug hotkeys. (Only use if you know what you are doing!)"
		}),
		options = options_enable,
		default = false,
	},

	Breaker(translate({en = "For Dedicated Servers", zh = "多(三层以上)世界选项,不要乱改"})),
	{
		name = "forestid",
		label = translate({
			en = "Forest Shard ID",
			zh = "森林世界ID"
		}),
        hover = translate({
			en = "Using Seaworthy in Shipwrecked Shard will send players to Forest Shard with corresponding ID.",
			zh = "使用海洋之椅时会将在海难的玩家传送到所选的id对应的世界"
		}),
        options =
		{
			{description = "Disabled", data = false},
			{description = "1", data = "1"},
			{description = "2", data = "2"},
			{description = "3", data = "3"},
			{description = "4", data = "4"},
			{description = "5", data = "5"},
		},
		default = false,
	},
	{
		name = "caveid",
		label = translate({
			en = "Cave Shard ID",
			zh = "洞穴世界ID"
		}),
        hover = translate({
			en = "Using Cave Entrances in Forest Shard will send players to Cave Shard with corresponding ID.",
			zh = "使用洞穴时会将玩家传送到所选的id对应的世界"
		}),
        options =
		{
			{description = "Disabled", data = false},
			{description = "1", data = "1"},
			{description = "2", data = "2"},
			{description = "3", data = "3"},
			{description = "4", data = "4"},
			{description = "5", data = "5"},
		},
		default = false,
	},
	{
		name = "shipwreckedid",
		label = translate({
			en = "Shipwrecked Shard ID",
			zh = "海难世界ID"
		}),
        hover = translate({
			en = "Using Seaworthy in Forest Shard will send players to Shipwrecked Shard with corresponding ID.",
			zh = "使用海洋之椅时会将在联机世界的玩家传送到所选的id对应的世界"
		}),
        options =
		{
			{description = "Disabled", data = false},
			{description = "1", data = "1"},
			{description = "2", data = "2"},
			{description = "3", data = "3"},
			{description = "4", data = "4"},
			{description = "5", data = "5"},
		},
		default = false,
	},
	{
		name = "volcanoid",
		label = translate({
			en = "Volcano Shard ID",
			zh = "火山世界ID"
		}),
        hover = translate({
			en = "Travelling via Volcano will send player to Volcano Shard with corresponding ID.",
			zh = "使用火山时会将玩家传送到所选的id对应的世界"
		}),
        options =
		{
			{description = "Disabled", data = false},
			{description = "1", data = "1"},
			{description = "2", data = "2"},
			{description = "3", data = "3"},
			{description = "4", data = "4"},
			{description = "5", data = "5"},
		},
		default = false,
	},
    	-- {
		-- name = "codename",
		-- label = "Fancy Name",
        -- hover = "This sentence explains the option in greater detail.",
		-- options =
		-- {
			-- {description = "Disabled", data = false},
			-- {description = "Enabled", data = true},
		-- },
		-- default = false,
	-- },
}
